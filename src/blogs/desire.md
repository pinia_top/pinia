---
icon: blog
title: 祝愿
date: 2023-05-05
star: 1
category:
  - Blog
tag:
  - Blog
---

不必祝我顺遂，不必于我无忧高枕，祝我独行八百里天堑，信步温吞，祝我千山万壑中回身，做世间第一等，此间最上乘，与君共勉。
