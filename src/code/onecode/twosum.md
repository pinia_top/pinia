---
title: 02.俩数之和
date: 2023-04-25 22:20:00
category:
  - code
---

## Code

::: tip 题目
给定一个整数数组`nums`和一个整数目标值`target`，请你在该数组中找出和为目标值`target`的那`两个`整数，并返回它们的数组下标。
你可以假设每种输入只会对应一个答案。但是，数组中同一个元素在答案里不能重复出现。
你可以按任意顺序返回答案

- 输入：nums = [2,7,11,15], target = 9
- 输出：[0,1]
- 解释：因为 nums[0] + nums[1] == 9 ，返回 [0, 1] 。
  :::

**base**

```js
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
const twoSum = (nums, target) => {
	const arr = [];
	for (let i = 0; i <= nums.length; i++) {
		for (let j = 1; j <= nums.length; j++) {
			if (nums[i] + nums[j] == target) {
				arr[0] = i;
				arr[1] = j;
			}
		}
	}
	return arr;
};
```

**Optimization**

```js
/**
 * @param {number[]} nums
 * @param {number} target
 * @return {number[]}
 */
const twoSum = (nums, target) => {
	const prevNums = {}; // 存储出现过的数字，和对应的索引

	for (let i = 0; i <= nums.length; i++) {
		// 遍历元素
		const curNum = nums[i]; // 当前元素
		const targetNum = target - curNum; // 满足要求的目标元素
		const targetNumIndex = prevNums[targetNum]; // 在prevNums中获取目标元素的索引
		if (targetNumIndex != undefined) {
			// 如果存在，直接返回 [目标元素的索引,当前索引]
			return [targetNumIndex, i];
		} else {
			// 如果不存在，说明之前没出现过目标元素
			prevNums[curNum] = i; // 存入当前的元素和对应的索引
		}
	}
};
```

**Advanced**

```js

```

## 思想

### top1

俩次 for 循环找到所需要的 target

### top2

- 用 hashMap 存储遍历过的元素和对应的索引。
- 每遍历一个元素，看看 hashMap 中是否存在满足要求的目标数字。
- 所有事情在一次遍历中完成（用了空间换取时间）。
