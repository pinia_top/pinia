---
title: 数组去重
date: 2023-05-07
---

```js
const arr = [1, 1, "1", 17, true, true, false, false, "true", "a", {}, {}];
// => [1, '1', 17, true, false, 'true', 'a', {}, {}]
```

## 利用 Set

```js
const res1 = Array.from(new Set(arr));
```

## 两层 for 循环+splice

```js
const arr2 = [1, 1, "1", 17, true, true, false, false, "true", "a", {}, {}];
const unique1 = (arr) => {
	let len = arr.length;
	for (let i = 0; i < len; i++) {
		for (let j = i + 1; j < len; j++) {
			if (arr[i] === arr[j]) {
				arr.splice(j, 1); // 每删除一个树，j--保证j的值经过自加后不变。同时，len--，减少循环次数提升性能
				len--;
				j--;
			}
		}
	}
	return arr;
};
```

## 利用 indexOf || include

```js
// indexOf
const unique2 = (arr) => {
	const res = [];
	for (let i = 0; i < arr.length; i++) {
		if (res.indexOf(arr[i]) === -1) res.push(arr[i]);
	}
	return res;
};

// include
const unique3 = (arr) => {
	const res = [];
	for (let i = 0; i < arr.length; i++) {
		if (!res.includes(arr[i])) res.push(arr[i]);
	}
	return res;
};
```

## 利用 Map

```js
const unique4 = (arr) => {
	const map = new Map();
	const res = [];
	for (let i = 0; i < arr.length; i++) {
		if (!map.has(arr[i])) {
			map.set(arr[i], true);
			res.push(arr[i]);
		}
	}
	return res;
};
```
