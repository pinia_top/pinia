---
title: 数组扁平化
date: 2023-05-07
---

数组扁平化是指将一个多维数组变为一个一维数组

```js
const arr = [1, [2, [3, [4, 5]]], 6]; // => [1, 2, 3, 4, 5, 6]
```

## 使用 flat

```js
const res1 = arr.flat(Infinity);
```

## 使用正则

```js
const res2 = JSON.stringify(arr).replace(/\[|\]/g, "").split(",");
```

## 使用 reduce

```js
const flatten = (arr) => {
	return arr.reduce((pre, cur) => {
		return pre.concat(Array.isArray(cur) ? flatten(cur) : cur);
	}, []);
};
const res3 = flatten(arr);
```

## 函数递归

```js
const res4 = [];
const fn = (arr) => {
	for (let i = 0; i < arr.length; i++) {
		if (Array.isArray(arr[i])) {
			fn(arr[i]);
		} else {
			res4.push(arr[i]);
		}
	}
};
fn(arr);
```

```js
let a = [1, 2, 3, [1, 2, 3, [1, 2, 3]]];
// 变成
let a = [1, 2, 3, 1, 2, 3, 1, 2, 3];
// 具体实现
function flat(arr = [], result = []) {
	arr.forEach((v) => {
		if (Array.isArray(v)) {
			result = result.concat(flat(v, []));
		} else {
			result.push(v);
		}
	});
	return result;
}
```
