---
title: 05. 画一个三角形
date: 2023-04-26 12:00:00
---

## 简版

css 画三角形的原理是利用盒子边框完成的,实现步骤可以分为以下四步:

1. 设置一个盒子
2. 设置四周不同颜色的边框
3. 将盒子宽高设置为 0，仅保留边框
4. 4.得到四个三角形，选择其中一个后，其他三角形（边框）设置颜色为透明

```css
div {
	width: 0px;
	height: 0px;
	border-top: 20px solid transparent;
	border-bottom: 20px solid red;
	border-left: 20px solid transparent;
	border-right: 20px solid transparent;
}
```

### 应用场景

在前端开发的时候，我们有时候会需要用到一个三角形的形状，比如地址选择或者播放器里面播放按钮

通常情况下，我们会使用图片或者 svg 去完成三角形效果图，但如果单纯使用 css 如何完成一个三角形呢？

实现过程似乎也并不困难，通过边框就可完成

### 空心三角行

`思想：`如果想要实现一个只有边框是空心的三角形，由于这里不能再使用 border 属性，所以最直接的方法是利用伪类新建一个小一点的三角形定位上去

```css
.border {
	width: 0;
	height: 0;
	border-style: solid;
	border-width: 0 50px 50px;
	border-color: transparent transparent #d9534f;
	position: relative;
}
.border:after {
	content: "";
	border-style: solid;
	border-width: 0 40px 40px;
	border-color: transparent transparent #96ceb4;
	position: absolute;
	top: 6px;
	left: -40px;
}
```
