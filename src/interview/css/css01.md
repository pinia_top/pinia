---
title: 01. css 新特性
date: 2023-04-26 12:00:00
---

## 简版

我知道的 C3 的新特性有很多，常见的如下:

- border-radius:圆角边框

- box-shadow:盒子阴影

- background-size:背景图片大小

- transition:过渡

- transform:转换(位移 旋转 缩放)

- animation:动画

- linear-gradient:线性渐变

- box-sizing:css3 盒子模型
