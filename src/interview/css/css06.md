---
title: 06. css选择器
date: 2023-04-26 12:00:00
---

## 简版

- css 选择器非常多,可以分为基础选择器 复合选择器,伪类选择器;伪元素选择器
- 基础选择器: id 选择器 ，class 类选择器，标签选择器，
- 复合选择器: 后代选择器，子代选择器，并集选择器,交集选择器

## 选择器

CSS 选择器用于选择要应用样式的 HTML 元素。

### 常见的 CSS 选择器有：

1. 标签选择器：通过标签名选择元素。例如，p 选择器选择所有段落元素。

```css
p {
	/* styles go here */
}
```

2. ID 选择器：通过元素的 ID 属性选择元素。ID 属性必须唯一。例如，#header 选择器选择具有 id="header"的元素。

```css
#header {
	/* styles go here */
}
```

3. 类选择器：通过元素的 class 属性选择元素。类可以在多个元素中共享。例如，.button 选择器选择具有 class="button"的元素。

```css
.button {
	/* styles go here */
}
```

4. 属性选择器：通过元素的属性选择元素。例如，[type="text"]选择器选择具有 type="text"属性的元素

```css
[type="text"] {
  /* styles go here */
}

[attribute] 选择带有attribute属性的元素
[attribute=value] 选择所有使用attribute=value的元素
[attribute~=value] 选择attribute属性包含value的元素
[attribute|=value]：选择attribute属性以value开头的元素
```

5. 后代选择器：选择嵌套在其他元素内部的元素。例如，ul li 选择器选择 ul 元素内部的所有 li 元素。

```css
ul li {
	/* styles go here */
}
```

6. 子选择器：选择作为父元素直接子元素的元素。例如，ul > li 选择器选择 ul 元素的直接子元素 li。

```css
ul > li {
	/* styles go here */
}
```

7. 相邻同胞选择器（.one+.two），选择紧接在.one 之后的所有.two 元素

```css
.one + .two {
	/* styles go here */
}
```

8. 群组选择器（div,p），选择 div、p 的所有元素

```css
div,
span {
	/* styles go here */
}
```

### 相对不常用的选择器

1. 伪类选择器

```css
:link ：选择未被访问的链接
:visited：选取已被访问的链接
:active：选择活动链接
:hover ：鼠标指针浮动在上面的元素
:focus ：选择具有焦点的

:first-of-type 表示一组同级元素中其类型的第一个元素
:last-of-type 表示一组同级元素中其类型的最后一个元素

:only-of-type 表示没有同类型兄弟元素的元素
:only-child 表示没有任何兄弟的元素

:nth-child(n) 根据元素在一组同级中的位置匹配元素
:nth-last-of-type(n) 匹配给定类型的元素，基于它们在一组兄弟元素中的位置，从末尾开始计数

:first-child：表示一组兄弟元素中的第一个元素
:last-child 表示一组兄弟元素中的最后一个元素

:root 设置HTML文档
:empty 指定空的元素
:enabled 选择可用元素
:disabled 选择被禁用元素
:checked 选择选中的元素
:not(selector) 选择与 <selector> 不匹配的所有元素
```

2. 伪元素选择器

```css
::first-letter ：用于选取指定选择器的首字母
::first-line ：选取指定选择器的首行
::before : 选择器在被选元素的内容前面插入内容
::after : 选择器在被选元素的内容后面插入内容
::selection : 选中用户选择的文本部分。
```

3.层次选择器

```css
/* h2 ~ p选择器选择h2元素之后的所有p元素。 */
h2 ~ p {
	/* styles go here */
}
```

## 优先级

`!important > 内联 > ID选择器 > 类选择器 > 标签选择器 > 通配符选择器 > 属性继承`

::: tip 注意
!important 声明，因为它可能会导致 CSS 样式难以维护和重构。优先级也应该根据具体情况进行调整和平衡，以确保样式的合理应用。
:::

## 继承属性

在 css 中，继承是指的是给父元素设置一些属性，后代元素会自动拥有这些属性

1. 字体系列属性

```css
font:组合字体
font-family:规定元素的字体系列
font-weight:设置字体的粗细
font-size:设置字体的尺寸
font-style:定义字体的风格
font-variant:偏大或偏小的字体
```

2. 文本系列属性

```css
text-indent：文本缩进
text-align：文本水平对刘
line-height：行高
word-spacing：增加或减少单词间的空白
letter-spacing：增加或减少字符间的空白
text-transform：控制文本大小写
direction：规定文本的书写方向
color：文本颜色
```

3. 元素可见性

```css
visibility
```

4. 表格布局属性

```css
caption-side：定位表格标题位置
border-collapse：合并表格边框
border-spacing：设置相邻单元格的边框间的距离
empty-cells：单元格的边框的出现与消失
table-layout：表格的宽度由什么决定
```

5. 列表属性

```css
list-style-type：文字前面的小点点样式
list-style-position：小点点位置
list-style：以上的属性可通过这属性集合
```

6. 引用

```css
quotes：设置嵌套引用的引号类型
```

7. cursor：箭头可以变成需要的形状

```css
cursor：箭头可以变成需要的形状
```

## 无继承的属性

1. display
2. 文本属性：vertical-align、text-decoration
3. 盒子模型的属性：宽度、高度、内外边距、边框等
4. 背景属性：背景图片、颜色、位置等
5. 定位属性：浮动、清除浮动、定位 position 等
6. 生成内容属性：content、counter-reset、counter-increment
7. 轮廓样式属性：outline-style、outline-width、outline-color、outline
8. 页面样式属性：size、page-break-before、page-break-after
