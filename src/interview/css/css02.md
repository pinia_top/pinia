---
title: 02. BFC
date: 2023-04-26 12:00:00
---

## BFC 简版

我是这样理解的:BFC 的中文意思是块级格式化上下文,是用于布局块级盒子的独立渲染区域，一个创建了新的 BFC 的盒子是独立布局的，盒子内元素的布局 不会影响盒子外面的元素。简单来说就是 BFC 就是 css 的一个布局概念，是一个独立的区域(容器) 满足下列条件之一就可以触发 BFC:

- HTML 根元素
- position 为 absolute 或 fixed
- float 属性不为 none（常用）
- overflow 不为 visible（常用）
- display 为 inline-block， table-cell， table-caption， flex（常用)

### 应用场景

主要作用

- margin 重叠
- 清除浮动
- 自适应布局.

### BFC 是什么？

BFC（Block Formatting Context），即块级格式化上下文，它是页面中的一块渲染区域，并且有一套属于自己的渲染规则：

- 内部的盒子会在垂直方向上一个接一个的放置
- 对于同一个 BFC 的俩个相邻的盒子的 margin 会发生重叠，与方向无关。
- 每个元素的左外边距与包含块的左边界相接触（从左到右），即使浮动元素也是如此
- BFC 的区域不会与 float 的元素区域重叠
- 计算 BFC 的高度时，浮动子元素也参与计算
- BFC 就是页面上的一个隔离的独立容器，容器里面的子元素不会影响到外面的元素，反之亦然
- BFC 目的是形成一个相对于外界完全独立的空间，让内部的子元素不会影响到外部的元素

### 触发条件

可以通过如下的设置来触发（产生）一个 BFC ：

- 根元素，即 HTML 元素
- 浮动元素：float 值为 left、right
- overflow 值不为 visible，为 auto、scroll、hidden
- display 的值为 inline-block、inltable-cell、table-caption、table、inline-table、flex、inline-flex、grid、inline-grid
- position 的值为 absolute 或 fixed
