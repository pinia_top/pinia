---
title: 03. 盒子模型
date: 2023-04-26 12:00:00
---

## 简版

盒子模型组成有 4 部分,分别为:内容 内边距 外边距(一般不计入盒子实际宽度) 边框

盒子模型有 2 种:标准盒子模型与怪异盒子模型

`W3C`标准盒子模型=content(内容)+border(边框)+padding(内边距)

`IE`怪异盒子模型=content(内容)(已经包含了 padding 和 border)

css3 种可以通过设置 box-sizing 属性来完成标准或许怪异盒子模型之间的切换,怪异盒子模型:box-sizing: border-box;标准盒子模型:box-sizing:content-box

## 代码示例

```css
.box {
	width: 200px;
	height: 100px;
	margin: 10px;
	padding: 10px;
	border: 1px solid #000;

	/* 标准盒子 */
	box-sizing: content-box;
	/* 怪异盒子 */
	box-sizing: border-box;
	/* 继承父元素的 box-sizing 属性值 */
	box-sizing: inherit;
}
```

