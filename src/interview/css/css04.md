---
title: 04. flexbox
date: 2023-04-26 12:00:00
---

## 简版

1. flexbox 是一种布局模型,让网页布局更简单灵活,基于 Flex 精确灵活控制块级盒子的布局方式，避免浮动布局中脱离文档流现象发生;给父元素添加 display: flex，子元素可以自动的挤压或拉伸;组成部分有弹性容器 弹性盒子 主轴和侧抽.
2. 应用场景:我们通过 flex 布局可以实现元素水平垂直居中以及左右固定,中间自适应;移动端小程序开发都建议使用 flex 布局.

### flexbox 是什么？

- Flexbox（弹性布局）是一种用于在 Web 页面中创建灵活，响应式布局的 CSS 布局模式。它允许您轻松地对元素进行布局，而不必担心它们的大小或位置如何影响其他元素。Flexbox 布局是基于容器和项目之间的关系，而不是传统的基于块和内联框模型的布局。
- 使用 Flexbox 布局，您可以在容器中定义弹性项目，这些项目可以根据容器的大小和其他属性自动调整其大小和位置。您可以设置项目的对齐方式，包括垂直和水平对齐，还可以控制它们的排序和间距。
- Flexbox 是一个非常强大的布局模式，可以用于创建各种不同的布局，例如居中元素、均匀分布元素、响应式布局等。

### 应用场景

- 在以前的文章中，我们能够通过 flex 简单粗暴的实现元素水平垂直方向的居中，以及在两栏三栏自适应布局中通过 flex 完成，这里就不再展开代码的演示

- 包括现在在移动端、小程序这边的开发，都建议使用 flex 进行布局

### 属性

常见的属性分为容器属性和容器成员属性

常见的容器属性有：

- flex-direction `决定主轴的方向(即项目的排列方向)`
  - row（默认值）：主轴为水平方向，起点在左端
  - row-reverse：主轴为水平方向，起点在右端
  - column：主轴为垂直方向，起点在上沿。
  - column-reverse：主轴为垂直方向，起点在下沿
- flex-wrap `弹性元素永远沿主轴排列，那么如果主轴排不下，通过flex-wrap决定容器内项目是否可换行`
  - nowrap（默认值）：不换行
  - wrap：换行，第一行在下方
  - wrap-reverse：换行，第一行在上方
- flex-flow `是flex-direction属性和flex-wrap属性的简写形式，默认值为row nowrap`
- justify-content `定义了项目在主轴上的对齐方式`
  - flex-start（默认值）：左对齐
  - flex-end：右对齐
  - center：居中
  - space-between：两端对齐，项目之间的间隔都相等
  - space-around：两个项目两侧间隔相等
- align-items `定义项目在交叉轴上如何对齐`

  - flex-start：交叉轴的起点对齐
  - flex-end：交叉轴的终点对齐
  - center：交叉轴的中点对齐
  - baseline: 项目的第一行文字的基线对齐
  - stretch（默认值）：如果项目未设置高度或设为 auto，将占满整个容器的高度

- align-content `定义了多根轴线的对齐方式。如果项目只有一根轴线，该属性不起作用`
  - flex-start：与交叉轴的起点对齐
  - flex-end：与交叉轴的终点对齐
  - center：与交叉轴的中点对齐
  - space-between：与交叉轴两端对齐，轴线之间的间隔平均分布
  - space-around：每根轴线两侧的间隔都相等。所以，轴线之间的间隔比轴线与边框的间隔大一倍
  - stretch（默认值）：轴线占满整个交叉轴

常见的容器成员属性：

- order `定义项目的排列顺序。数值越小，排列越靠前，默认为 0`

```css
.item {
	order: <integer>;
}
```

- flex-grow `上面讲到当容器设为flex-wrap: nowrap;不换行的时候，容器宽度有不够分的情况，弹性元素会根据flex-grow来决定`

```css
/* 如果所有项目的flex-grow属性都为 1，则它们将等分剩余空间（如果有的话） */
/* 如果一个项目的flex-grow属性为 2，其他项目都为 1，则前者占据的剩余空间将比其他项多一倍 */
/* 弹性容器的宽度正好等于元素宽度总和，无多余宽度，此时无论flex-grow是什么值都不会生效 */
.item {
	flex-grow: <number>;
}
```

- flex-shrink `定义了项目的缩小比例（容器宽度<元素总宽度时如何收缩），默认为 1，即如果空间不足，该项目将缩小`

```css
/* 如果所有项目的flex-shrink属性都为 1，当空间不足时，都将等比例缩小 */
/* 如果一个项目的flex-shrink属性为 0，其他项目都为 1，则空间不足时，前者不缩小  */
/* 在容器宽度有剩余时，flex-shrink也是不会生效的 */
.item {
	flex-shrink: <number>; /* default 1 */
}
```

- flex-basis `设置的是元素在主轴上的初始尺寸，所谓的初始尺寸就是元素在flex-grow和flex-shrink生效前的尺寸`

```css
/* 浏览器根据这个属性，计算主轴是否有多余空间，默认值为auto，即项目的本来大小，如设置了width则元素尺寸由width/height决定（主轴方向），没有设置则由内容决定 */
/* 当设置为 0 的是，会根据内容撑开 */
/* 它可以设为跟width或height属性一样的值（比如 350px），则项目将占据固定空间 */
.item {
	flex-basis: <length> | auto; /* default auto */
}
```

- flex `flex属性是flex-grow, flex-shrink 和 flex-basis的简写，默认值为0 1 auto，也是比较难懂的一个复合属性`

```css
.item {
	flex: none | [ < "flex-grow" > < "flex-shrink" >? || < "flex-basis" >];
}
```

- align-self `允许单个项目有与其他项目不一样的对齐方式，可覆盖align-items属性`

```css
/* 默认值为auto，表示继承父元素的align-items属性，如果没有父元素，则等同于stretch */
.item {
	align-self: auto | flex-start | flex-end | center | baseline | stretch;
}
```
