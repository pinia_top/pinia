---
title: 07. 相对单位/绝对单位
date: 2023-04-26 12:00:00
---

## 简版

web 端单位大致可以分为 相对单位 和 绝对单位

1. px：绝对单位，页面按精确像素展示
2. em：相对单位，基准点为父节点字体的大小，如果自身定义了 font-size 按自身来计算，整个页面内 1em 不是一个固定的值
3. rem：相对单位，可理解为 root em, 相对根节点 html 的字体大小来计算
4. vh、vw：主要用于页面视口大小布局，在页面布局上更加方便简单

### 相对单位：

1. em

em 是相对长度单位。相对于当前对象内文本的字体尺寸。如当前对行内文本的字体尺寸未被人为设置，则相对于浏览器的默认字体尺寸（1em = 16px）
为了简化 font-size 的换算，我们需要在 css 中的 body 选择器中声明 font-size= 62.5%，这就使 em 值变为 16px\*62.5% = 10px
这样 12px = 1.2em, 10px = 1em, 也就是说只需要将你的原来的 px 数值除以 10，然后换上 em 作为单位就行了

```css
<style>
    html {font-size: 10px;  } /*  公式16px*62.5%=10px  */
</style>
```

特点：

- em 的值并不是固定的
- em 会继承父级元素的字体大小
- em 是相对长度单位。相对于当前对象内文本的字体尺寸。如当前对行内文本的字体尺寸未被人为设置，则相对于浏览器的默认字体尺寸
- 任意浏览器的默认字体高都是 16px

2. rem
   rem，相对单位，相对的只是 HTML 根元素 font-size 的值
   同理，如果想要简化 font-size 的转化，我们可以在根元素 html 中加入`font-size: 62.5%`

```css
<style>
    html {font-size: 62.5%;  } /*  公式16px*62.5%=10px  */
</style>
```

特点：

- rem 单位可谓集相对大小和绝对大小的优点于一身
- 和 em 不同的是 rem 总是相对于根元素，而不像 em 一样使用级联的方式来计算尺寸

3. vh/vm
   vw ，就是根据窗口的宽度，分成 100 等份，100vw 就表示满宽，50vw 就表示一半宽。（vw 始终是针对窗口的宽），同理，vh 则为窗口的高度
   这里的窗口分成几种情况：

- 在桌面端，指的是浏览器的可视区域
- 移动端指的就是布局视口

像 vw、vh，比较容易混淆的一个单位是%，不过百分比宽泛的讲是相对于父元素：

- 对于普通定位元素就是我们理解的父元素
- 对于 position: absolute;的元素是相对于已定位的父元素
- 对于 position: fixed;的元素是相对于 ViewPort（可视窗口）

4. rpx uniapp

### 绝对单位：

1. px:

px，表示像素，所谓像素就是呈现在我们显示器上的一个个小点，每个像素点都是大小等同的，所以像素为计量单位被分在了绝对长度单位中

有些人会把 px 认为是相对长度，原因在于在移动端中存在设备像素比，px 实际显示的大小是不确定的

这里之所以认为 px 为绝对单位，在于 px 的大小和元素的其他属性无关

2. upx: uniapp
