import { hopeTheme } from "vuepress-theme-hope";
import { zhNavbar } from "./navbar/index";
import { zhSidebar } from "./sidebar/index";
import { cut } from "nodejs-jieba";

export default hopeTheme({
	navbarLayout: {
		start: ["Brand"],
		center: ["Links"],
		end: ["Language", "Repo", "Outlook", "Search"],
	},
	// 主题配置
	author: {
		// 作者姓名
		name: "pinia",
		// 作者网站
		url: "https://pinia.top",
		// 作者 Email
		email: "6668682@gmail.com",
	},

	breadcrumb: true,

	// 站点图标
	favicon: "/assets/favicon/favicon.ico",

	iconAssets: "iconfont",

	logo: "/assets/avatar/logo.png",

	docsDir: "src",

	repo: "https://gitee.com/pinia_top/pinia",

	// 全屏按钮
	fullscreen: true,

	//深色模式
	darkmode: "switch",

	//打印按钮
	print: true,

	//纯净模式
	// pure: true,

	// RTL 布局
	// rtl: true,

	//侧边栏
	sidebar: "heading",

	blog: {
		medias: {
			BiliBili: "https://space.bilibili.com/5115515",
			QQMusic: "https://y.qq.com/n/ryqq/profile/like/song",
			Email: "200432@163.com",
			// Evernote: "https://example.com",
			// Facebook: "https://example.com",
			// Flipboard: "https://example.com",
			Gitee: "https://gitee.com/ToSum",
			GitHub: "https://github.com/44NF",
			// Gitlab: "https://example.com",
			Gmail: "gamil:6668682@egmail.com",
			Instagram: "https://www.instagram.com/pinia_top/",

			Twitter: "https://twitter.com/piniatop",

			Weibo: "https://weibo.com/u/6120861308",
		},
	},

	locales: {
		/**
		 * Chinese locale config
		 */
		"/": {
			// navbar
			navbar: zhNavbar,

			// sidebar
			sidebar: zhSidebar,

			footer: "pinia",

			displayFooter: true,

			blog: {
				description: "一名前端开程序员，考公ing...",
				intro: "/intro.html",
			},

			// page meta
			metaLocales: {
				editLink: "在 MarkDown 上编辑此页",
			},
		},
	},

	encrypt: {
		config: {
			"/interview/": ["1314"],
			"/intro.html": "17603404200",
		},
	},

	plugins: {
		blog: true,

		photoSwipe: true,

		copyCode: {
			// 移动端展示复制按钮
			showInMobile: true,
			//复制成功的提示
			duration: 10000,
		},
		//版权信息
		copyright: true,
		//代码主题
		prismjs: {
			light: "ateliersulphurpool-light",
			dark: "atom-dark",
		},
		//评论
		comment: {
			provider: "Waline",
			serverURL: "https://ycbzbkmu.api.lncldglobal.com",
		},
		//MarkDown增强
		mdEnhance: {
			// 添加选项卡支持
			tabs: true,
			//代码组分块
			codetabs: true,
			//chart
			chart: true,
			//Echats
			echarts: true,
			// 启用 GFM 警告
			alert: true,
			// 启用 kotlin 交互演示
			kotlinPlayground: true,
			// mermaid
			mermaid: true,
			// 使用 KaTeX 启用 TeX 支持
			katex: true,
			// 启用 vue 交互演示
			vuePlayground: true,
			// 启用下角标功能
			sub: true,
			// 启用上角标
			sup: true,
			// 在此放置交互演示配置
			playground: {
				// 添加预设
				presets: [
					"ts",
					"vue",
					"unocss",
					{
						name: "playground#language",
						component: "PlaygroundComponent",
						propsGetter: (playgroundData: any): Record<string, string> => ({
							// 交互演示属性
						}),
					},
				],
				// 设置内置预设 (可选)
				config: {
					ts: {
						// ...
					},
					vue: {
						// ...
					},
					unocss: {
						// ...
					},
				},
			},
			//代码演示
			demo: true,
			// 任务列表
			tasklist: true,
			//**** */
			//图片懒加载
			// 启用 figure
			figure: true,
			// 启用图片懒加载
			imgLazyload: true,
			// 启用图片标记
			imgMark: true,
			// 启用图片大小
			imgSize: true,
			//导入文件
			include: true,
			//属性支持
			attrs: true,
			//幻灯片
			revealJs: true,
			// [提示容器]这就是默认选项，所以你可以直接使用它
			hint: true,
			//标记
			mark: true,
			//样式化
			stylize: [
				// 选项
				{
					matcher: "Recommended",
					replacer: ({ tag }) => {
						if (tag === "em")
							return {
								tag: "Badge",
								attrs: { type: "tip" },
								content: "Recommended",
							};
					},
				},
				{
					matcher: /^不/,
					replacer: ({ tag, attrs, content }) => {
						if (tag === "em")
							return {
								tag: "span",
								attrs: { ...attrs, style: "color: red" },
								content,
							};
					},
				},
			],
			//流程图
			flowchart: true,
			//脚注
			footnote: true,
			// 自定义对齐
			align: true,
		},

		//搜索
		// searchPro: true,
		searchPro: {
			//插件选项
			indexContent: true,
			//是否自动提示搜索建议。
			autoSuggestions: false,
			queryHistoryCount: 0,
			resultHistoryCount: 0,
			searchDelay: 150,
			customFields: [
				{
					getter: ({ frontmatter }) => frontmatter.category as string[],
					formatter: {
						"/": "分类: $content",
					},
				},
				{
					getter: ({ frontmatter }) => frontmatter.tag as string[],
					formatter: {
						"/": "标签: $content",
					},
				},
			],
			indexOptions: {
				tokenize: (text, fieldName) =>
					fieldName === "id" ? [text] : cut(text, true),
			},
		},
	},
});
