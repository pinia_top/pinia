import { navbar } from "vuepress-theme-hope";

export const zhNavbar = navbar([
	// web
	{
		text: "Web",
		icon: "window",
		prefix: "/web/",
		children: [
			{
				text: "基础",
				icon: "repo",
				link: "base/",
			},
			{ text: "进阶", icon: "repo", link: "table/" },
			{ text: "强化", icon: "repo", link: "strengthen/" },
		],
	},
	//z7z8
	{
		text: "Z7z8",
		icon: "any",
		children: [
			{
				text: "杂七杂八的知识",
				prefix: "/clutter/",
				children: [
					{
						text: "python",
						icon: "python",
						link: "python/",
					},
					{
						text: "读书感悟",
						icon: "write",
						link: "study/",
					},
					{
						text: "Three.js",
						icon: "customize",
						link: "threejs/",
					},
					{
						text: "Tailwindcss",
						icon: "css",
						link: "tailwindcss/",
					},
					{
						text: "canvas",
						icon: "context",
						link: "canvas/",
					},
					{
						text: "svg",
						icon: "svg",
						link: "svg/",
					},
					{
						text: "nodeJS",
						icon: "nodeJS",
						link: "node/",
					},
					{
						text: "TypeScript",
						icon: "typescript",
						link: "typescript/",
					},
				],
			},
			{
				text: "你所不知道的知识",
				prefix: "/skill/",
				children: [
					{
						text: "你所不知道的CSS",
						icon: "css",
						link: "css/",
					},
					{ text: "你所不知道的HTML", icon: "html", link: "html/" },
					{
						text: "你所不知道的JavaScript",
						icon: "javascript",
						link: "JavaScript/",
					},
					{
						text: "你所不知道的http",
						icon: "http",
						link: "http/",
					},
					{
						text: "你所不知道的Style",
						icon: "style",
						link: "style/",
					},
					{
						text: "你所不知道的Vue",
						icon: "vue",
						link: "vue/",
					},
					{
						text: "你所不知道的three.js",
						icon: "customize",
						link: "threejs/",
					},
					{
						text: "你所不知道的Bug",
						icon: "debug",
						link: "bug/",
					},
				],
			},
		],
	},
	// 面试宝典
	{
		text: "Job",
		icon: "list",
		prefix: "/interview/",
		children: [
			{
				text: "HTML",
				icon: "html",
				link: "html/html01",
			},
			{
				text: "CSS",
				icon: "css",
				link: "css/css01",
			},
			{
				text: "JS",
				icon: "javascript",
				link: "js/js01",
			},
			{
				text: "VUE",
				icon: "vue",
				link: "vue/vue01",
			},
			{
				text: "Git",
				icon: "git",
				link: "git/git01",
			},
			{
				text: "Write",
				icon: "write",
				link: "write/write01",
			},
			{
				text: "简历",
				icon: "emoji",
				link: "z7z8/z7z801",
			},

			// { text: "git", link: "/docs/git/" },
			// { text: "webpack", link: "/docs/webpack/" },
			// { text: "network", link: "/docs/network/" },
			// { text: "ts", link: "/docs/ts/" },
			// { text: "nodejs", link: "/docs/nodejs/" },
			// { text: "datastructures", link: "/docs/datastructures/" },
			// { text: "react", link: "/docs/react/" },
			// { text: "chrome", link: "/docs/chrome/" },
			// { text: "linux", link: "/docs/linux/" },
			// { text: "applet", link: "/docs/applet/" },
			// { text: "designpattern", link: "/docs/designpattern/" },
		],
	},
	// code
	// {
	// 	text: "code",
	// 	icon: "template",
	// 	prefix: "/code/",
	// 	children: [
	// 		{ text: "算法", link: "algorithm/", icon: "back-stage" },
	// 		{ text: "每日一Code", link: "onecode/", icon: "code" },
	// 	],
	// },
	// 博客
	{
		text: "Blog",
		icon: "blog",
		link: "/blogs/",
	},
	//开发文档
	{
		text: "Note",
		icon: "note",
		children: [
			{
				text: "工具",
				icon: "config",
				children: [
					{
						text: "语雀",
						icon: "folder",
						link: "https://www.yuque.com/dashboard",
					},
					{
						text: "有道云笔记",
						icon: "folder",
						link: "https://note.youdao.com/web/#/file/recent/note/WEB9076247e4d384bfabd4d58d9877f444a/",
					},
					{
						text: "Apifox",
						icon: "apifox",
						link: "https://apifox.com/",
					},
				],
			},
			{
				text: "打包工具",
				children: [
					{
						text: "WebPack",
						icon: "file",
						link: "https://www.webpackjs.com/",
					},
					{
						text: "Vite",
						icon: "file",
						link: "https://cn.vitejs.dev/",
					},
					{
						text: "parcel打包工具",
						icon: "file",
						link: "https://parceljs.org/",
					},
				],
			},
			{
				text: "文档",
				children: [
					{
						text: "Vue.js",
						icon: "vue",
						link: "https://cn.vuejs.org/",
					},
					{
						text: "uniapp",
						icon: "mini-app",
						link: "https://uniapp.dcloud.net.cn/collocation/pages.html#",
					},
					{
						text: "微信开发者工具",
						icon: "wechat",
						link: "https://developers.weixin.qq.com/doc/",
					},
					{
						text: "VueX",
						icon: "file",
						link: "https://vuex.vuejs.org/zh/guide/",
					},
					{
						text: "Pinia",
						icon: "file",
						link: "https://pinia.web3doc.top/introduction.html",
					},
					{
						text: "postcss",
						icon: "css",
						link: "https://postcss.org/",
					},
					{
						text: "RFC 7231",
						icon: "file",
						link: "https://www.rfc-editor.org/rfc/rfc7231.html",
					},

					{
						text: "NPM",
						icon: "npm",
						link: "https://www.npmjs.com/",
					},
					{
						text: "Ts",
						icon: "typescript",
						link: "https://typescript.bootcss.com/",
					},
					{
						text: "ts入门教程",
						icon: "typescript",
						link: "https://ts.xcatliu.com/",
					},
					{
						text: "Tailwindcss",
						icon: "css",
						link: "https://www.tailwindcss.cn/",
					},
					{
						text: "Harmonyos",
						icon: "object",
						link: "https://developer.harmonyos.com/",
					},
				],
			},
			{
				text: "Three.js",
				icon: "customize",
				children: [
					{
						text: "three.js",
						icon: "link",
						link: "https://threejs.org/",
					},
					{
						text: "gsap",
						icon: "link",
						link: "https://tweenmax.com.cn/index.html",
					},
					{
						text: "Texture-纹理",
						icon: "file",
						link: "https://texturelabs.org/",
					},
					{
						text: "Poliigon-贴图",
						icon: "file",
						link: "https://www.poliigon.com/",
					},
					{
						text: "3dtextures-贴图",
						icon: "file",
						link: "https://3dtextures.me/",
					},
					{
						text: "arroway-textures-贴图",
						icon: "file",
						link: "https://www.arroway-textures.ch/",
					},
					{
						text: "unrealengine-注册账号免费使用",
						icon: "file",
						link: "https://www.unrealengine.com/zh-CN/bridge",
					},
					{
						text: "pixabay-免费图片",
						icon: "file",
						link: "https://pixabay.com/zh/",
					},
					{
						text: "leiapix-3D图片生成",
						icon: "file",
						link: "https://convert.leiapix.com/",
					},
				],
			},
			{
				text: "canvas",
				children: [
					{
						text: "PixiJS",
						icon: "link",
						link: "https://pixijs.com/",
					},
				],
			},
			{
				text: "组件库",
				children: [
					{
						text: "Element-UI",
						icon: "link",
						link: "https://element.eleme.cn/#/zh-CN/component/installation",
					},
					{
						text: "Element Plus",
						icon: "link",
						link: "https://element-plus.gitee.io/zh-CN/component/button.html",
					},
					{
						text: "Ant Design",
						icon: "link",
						link: "https://www.antdv.com/components/overview-cn",
					},
					{
						text: "arco.design",
						link: "https://arco.design/react/docs/start",
					},
					{
						text: "Vant 4",
						icon: "link",
						link: "https://vant-contrib.gitee.io/vant/#/zh-CN",
					},
					{
						text: "Vant Weapp",
						icon: "link",
						link: "https://vant-contrib.gitee.io/vant-weapp/#/quickstart",
					},
					{
						text: "ECharts",
						icon: "link",
						link: "https://echarts.apache.org/handbook/zh/get-started/",
					},
					{
						text: "three.js",
						icon: "link",
						link: "http://www.webgl3d.cn/",
					},
				],
			},
			{
				text: "开发者网站",
				icon: "http",
				children: [
					{
						text: "有道智云·AI开放平台",
						icon: "folder",
						link: "https://ai.youdao.com/console/#/",
					},
					{
						text: "Gitee",
						icon: "gitee",
						link: "https://gitee.com/",
					},
					{
						text: "GitHub",
						icon: "github",
						link: "https://github.com/44NF",
					},
				],
			},
		],
	},
	//你所不知道的
	{
		text: "Adobe",
		icon: "linter",
		children: [
			{
				text: "GIMP",
				icon: "css",
				children: [
					{
						text: "GIMP",
						icon: "style",
						link:'adobe/gimp'
					},
					{
						text: "GIMPHelp",
						icon: "style",
						link: "https://gimphelp.org/",
					},
				],
			},
		],
	},
	//国家公务员考试
	{
		text: "Gov",
		icon: "ability",
		prefix: "/gov/",
		children: [
			{
				text: "资料分析",
				icon: "folder",
				link: "profile/",
			},
			{
				text: "言语理解与表达",
				icon: "folder",
				link: "speak/",
			},
			{
				text: "判断推理",
				icon: "folder",
				link: "judgment/",
			},
			{
				text: "数量关系",
				icon: "folder",
				link: "amount/",
			},
			{
				text: "常识",
				icon: "folder",
				link: "sense/",
			},
			{
				text: "申论",
				icon: "folder",
				link: "essay/",
			},
		],
	},
]);
