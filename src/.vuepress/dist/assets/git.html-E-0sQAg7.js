import{_ as a}from"./plugin-vue_export-helper-x3n3nnut.js";import{r as l,o as d,c,a as n,b as i,d as t,e}from"./app-fFzGerwm.js";const m={},r=e(`<h2 id="简版" tabindex="-1"><a class="header-anchor" href="#简版" aria-hidden="true">#</a> 简版</h2><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code>ssh -T git@gitee.com或ssh -T git@github.com         //git切换gitee和github
git init                                            //初始化
git clone &lt;https&gt;                                   //clone一个git仓库
git add .                                           //将文件添加到缓存
git commit -m <span class="token string">&quot;第一次版本提交&quot;</span>                       //将缓存区内容添加到仓库中
git branch                                          //查看分支命令
git branch &lt;branchname&gt;                             //创建分支命令
git checkout &lt;branchname&gt;                           //切换分支命令
git branch -b &lt;branchname&gt;                          //创建并切换分支命令
git branch -d &lt;branchname&gt;                          //删除分支命令
git merge &lt;branchname&gt;                              //合并分支命令
git push -u -f origin master:&lt;branchname&gt;           //第一次提交项目到master分支    -f：强制推送    -u:选项指定一个默认主机     branchname:分支别名
git pull                                            //从远程获取最新版本并merge到本地
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="命令" tabindex="-1"><a class="header-anchor" href="#命令" aria-hidden="true">#</a> 命令</h2><h3 id="将本地项目初始化" tabindex="-1"><a class="header-anchor" href="#将本地项目初始化" aria-hidden="true">#</a> 将本地项目初始化</h3><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code><span class="token comment"># 初始化 在当前目录新建一个Git代码库</span>
git init

<span class="token comment"># 新建一个目录，将其初始化为Git代码库</span>
git init &lt;project-name&gt;

<span class="token comment"># 给项目添加远程仓库</span>
git remote add origin git@github.xxx.git

<span class="token comment"># 本地分支和远程分支建立联系(使用git branch -vv  可以查看本地分支和远程分支的关联关系)</span>
git branch --set-upstream-to=origin/远程分支 本地分支
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="查看" tabindex="-1"><a class="header-anchor" href="#查看" aria-hidden="true">#</a> 查看</h3><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code><span class="token comment"># 查看工作目录和暂存区的状态</span>
git status

<span class="token comment"># 查看工作区与暂存区的差异</span>
git diff

<span class="token comment"># 查看提交历史</span>
git log

<span class="token comment"># 查看命令历史</span>
git reflog

<span class="token comment"># ------------------------分支------------------------</span>

<span class="token comment"># 查看git本地分支</span>
git branch

<span class="token comment"># 查看git远程分支</span>
git branch -r

<span class="token comment"># 查看所有本地分支和远程分支，远程分支为红色</span>
git branch -a

<span class="token comment"># 查看每个本地分支的最后一次提交</span>
git branch -v

<span class="token comment"># 查看本地分支和远程分支联系</span>
git branch -vv

<span class="token comment"># 切换git分支</span>
git checkout &lt;branch-name&gt;

<span class="token comment"># 创建分支</span>
git branch &lt;branch-name&gt;

<span class="token comment"># 新建并切换git分支</span>
git checkout -b &lt;branch-name&gt;

<span class="token comment"># 删除git分支</span>
git branch -d &lt;branch-name&gt;

<span class="token comment"># 强制删除git分支</span>
git branch -D &lt;branch-name&gt;

<span class="token comment"># 删除git远程分支</span>
git push origin -d &lt;branch-name&gt;

<span class="token comment"># 本地分支和远程分支建立联系</span>
git branch --set-upstream-to=origin/远程分支 本地分支
<span class="token comment"># ------------------------分支------------------------</span>

<span class="token comment"># 查看git 配置</span>
git config -l

<span class="token comment"># 修改远程仓库地址</span>
git remote set-url origin git@github.xxx.git

<span class="token comment"># 本地分支回滚到指定版本</span>
git reset --hard &lt;commit ID号&gt;

<span class="token comment"># 强制推送到远程分支</span>
git push -f origin &lt;branch name&gt;
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="删除-commit-记录" tabindex="-1"><a class="header-anchor" href="#删除-commit-记录" aria-hidden="true">#</a> 删除 commit 记录</h3><div class="hint-container info"><p class="hint-container-title">注意</p><p>何时需要删除 Git 提交的历史记录</p><ol><li>当历史记录中出现过密码等敏感信息在历史记录中, 需要删除历史记录时</li><li>当项目因历史记录过多, 导致历史记录占用了大量内存时, 比如 Github 仓库个人总容量时 1GB 不够用时</li><li>当你想要一个全新的项目的时候, 并且想保持项目代码不变</li></ol></div><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code>
<span class="token comment"># 创建全新的孤立分支 latest_branch</span>
git checkout --orphan &lt;latest_branch&gt;

<span class="token comment"># 2. 暂存所有文件</span>
git add -A

<span class="token comment"># 3. 提交所有文件的修改到latest_branch</span>
git commit -am <span class="token string">&quot;del all history&quot;</span>

<span class="token comment"># 4. 删除原来的master分支</span>
git branch -D master

<span class="token comment"># 5. 修改latest_branch分支名为master</span>
git branch -m master

<span class="token comment"># 6. 强制更新远程服务器的master分支, 至此清空git所有历史</span>
git push -f origin master
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="新建代码库" tabindex="-1"><a class="header-anchor" href="#新建代码库" aria-hidden="true">#</a> 新建代码库</h3><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code><span class="token comment"># 下载一个项目和它的整个代码历史</span>
git clone [url]

<span class="token comment"># 将alpha目录（必须是git代码仓库），克隆到delta目录</span>
<span class="token comment"># bare参数表示delta目录只有仓库区，没有工作区和暂存区，即delta目录中就是.git目录的内容</span>
git clone alpha delta --bare
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="仓库与-git-的配置" tabindex="-1"><a class="header-anchor" href="#仓库与-git-的配置" aria-hidden="true">#</a> 仓库与 git 的配置</h3><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code><span class="token comment"># 显示当前的Git配置</span>
git config --list

<span class="token comment"># 编辑Git配置文件</span>
git config -e [--global]

<span class="token comment"># 设置提交代码时的用户信息</span>
git config [--global] user.name <span class="token string">&quot;[name]&quot;</span>
git config [--global] user.email <span class="token string">&quot;[email address]&quot;</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="git-config" tabindex="-1"><a class="header-anchor" href="#git-config" aria-hidden="true">#</a> git config</h3><p>git 的设置文件为.gitconfig,它可以在用户主目录下，也可以在项目的目录之下</p><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code><span class="token comment"># 显示当前的Git配置</span>
git config --list
git config -l

<span class="token comment"># 编辑Git配置文件</span>
git config -e [--global]

<span class="token comment"># 设置提交代码时的用户信息</span>
<span class="token comment"># 参数</span>
<span class="token comment"># 1.系统级别：--system</span>
<span class="token comment"># 2.用户全局：--global</span>
<span class="token comment"># 3.单独一个项目：--local</span>
git config --global user.name <span class="token string">&quot;xxxx&quot;</span> #用户名
git config --global user.email <span class="token string">&quot;xxxx@xxx.com&quot;</span> #邮箱
git config --global core.editor vim #编辑器

git config --global alias.st status #按这种方法，配置别名
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="增加-删除文件" tabindex="-1"><a class="header-anchor" href="#增加-删除文件" aria-hidden="true">#</a> 增加/删除文件</h3><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code><span class="token comment"># 添加指定文件到暂存区</span>
git add [file1] [file2] ...

<span class="token comment"># 添加指定目录到暂存区，包括子目录</span>
git add [dir]

<span class="token comment"># 添加当前目录的所有文件到暂存区</span>
git add .

<span class="token comment"># 删除工作区文件，并且将这次删除放入暂存区</span>
git rm [file1] [file2] ...

<span class="token comment"># 停止追踪指定文件，但该文件会保留在工作区</span>
git rm --cached [file]

<span class="token comment"># 改名文件，并且将这个改名放入暂存区</span>
git mv [file-original] [file-renamed]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="git-add" tabindex="-1"><a class="header-anchor" href="#git-add" aria-hidden="true">#</a> git add</h3><ol><li><code>git add</code>命令用于将变化的文件，从工作区提交到暂存区。它的作用就是告诉 Git，下一次哪些变化需要保存到仓库区。</li><li>用户可以使用<code>git status</code>命令查看目前的暂存区放置了哪些文件。</li></ol><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code><span class="token comment"># 添加指定文件到暂存区</span>
<span class="token command">$ git add [file1] [file2] ...</span>

<span class="token comment"># 添加指定目录到暂存区，包括子目录</span>
<span class="token command">$ git add [dir]</span>

<span class="token comment"># 添加当前目录的所有文件到暂存区</span>
<span class="token comment"># 会把当前目录中所有有改动的文件（不包括.gitignore中要忽略的文件）都添加到git缓冲区以待提交</span>
<span class="token command">$ git add .</span>

<span class="token comment"># 会把当前目录中所有有改动的文件（包括.gitignore中要忽略的文件）都添加到git缓冲区以待提交</span>
<span class="token command">$ git add *		#&lt;不推荐&gt;</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ol start="3"><li>-u 参数表示只添加暂存区已有的文件（包括删除操作），但不添加新增的文件。</li></ol><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code>git add -u
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><ol start="4"><li>-A 或者--all 参数表示追踪所有操作，包括新增、修改和删除<code>Git 2.0 版开始，-A参数成为默认，即git add .等同于git add -A</code></li><li>-f 参数表示强制添加某个文件，不管.gitignore 是否包含了这个文件。</li></ol><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code><span class="token command">$ git add<span class="token parameter"> -f</span> &lt;fileName&gt;</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><ol start="6"><li>-p 参数表示进入交互模式，指定哪些修改需要添加到暂存区。即使是同一个文件，也可以只提交部分变动。</li></ol><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code><span class="token comment"># 添加每个变化前，都会要求确认，对于同一个文件的多处变化，可以实现分次提交</span>
<span class="token command">$ git add<span class="token parameter"> -p</span></span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><div class="hint-container tip"><p class="hint-container-title">注意</p><p>注意，Git 2.0 版以前，git add 默认不追踪删除操作。即在工作区删除一个文件后，git add 命令不会将这个变化提交到暂存区，导致这个文件继续存在于历史中。Git 2.0 改变了这个行为。</p></div><h3 id="代码提交-git-commit" tabindex="-1"><a class="header-anchor" href="#代码提交-git-commit" aria-hidden="true">#</a> 代码提交(git commit)</h3><p>git commit 命令用于将暂存区中的变化提交到仓库区。-m 参数用于指定 commit 信息，是必需的。如果省略-m 参数，git commit 会自动打开文本编辑器，要求输入。</p><ol><li>-m 参数用于添加提交说明,如果没有指定提交说明，运行 commit 会直接打开默认的文本编辑器，让用户撰写提交说明.</li><li>-a 参数用于先将所有工作区的变动文件，提交到暂存区，再运行 git commit。用了-a 参数，就不用执行 git add .命令了.</li><li>--allow-empty 参数用于没有提交信息的 commit.</li><li>–amend 参数用于撤销上一次 commit，然后生成一个新的 commit.</li><li>--fixup 参数的含义是，当前添加的 commit 是以前某一个 commit 的修正。以后执行互动式的 git rebase 的时候，这两个 commit 将会合并成一个。</li><li>--squash 参数的作用与--fixup 类似，表示当前添加的 commit 应该与以前某一个 commit 合并成一个，以后执行互动式的 git rebase 的时候，这两个 commit 将会合并成一个。</li></ol><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code><span class="token comment"># 提交暂存区到仓库区</span>
git commit -m [message]

<span class="token comment"># 提交暂存区的指定文件到仓库区</span>
git commit [file1] [file2] ... -m [message]

<span class="token comment"># 提交工作区自上次commit之后的变化，直接到仓库区</span>
git commit -a

<span class="token comment"># 提交时显示所有diff信息</span>
git commit -v

<span class="token comment"># 用于没有提交信息的 commit</span>
<span class="token command">$ git commit<span class="token parameter"> --allow</span>-empty</span>

<span class="token comment"># 使用一次新的commit，替代上一次提交</span>
<span class="token comment"># 如果代码没有任何新变化，则用来改写上一次commit的提交信息</span>
git commit --amend -m [message]

<span class="token comment"># 重做上一次commit，并包括指定文件的新变化</span>
git commit --amend &lt;file1&gt; &lt;file2&gt; ...

<span class="token comment"># 提交说明将自动生成，即在目标 commit 的提交说明的最前面，添加&quot;fixup!&quot;这个词</span>
git commit --fixup &lt;commit&gt;

</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="分支" tabindex="-1"><a class="header-anchor" href="#分支" aria-hidden="true">#</a> 分支</h3><p>新建一个分支，指向当前 commit。本质是在 refs/heads/目录中生成一个文件，文件名为分支名，内容为当前 commit 的哈希值</p><pre><code>::: tip 注意
注意：创建后，还是停留在原来分支，需要用 git checkout 切换到新建分支
:::
</code></pre><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code><span class="token comment"># 列出所有本地分支</span>
<span class="token command">$ git branch</span>

<span class="token comment"># 列出所有远程分支</span>
<span class="token command">$ git branch<span class="token parameter"> -r</span></span>

<span class="token comment"># 列出所有本地分支和远程分支</span>
<span class="token command">$ git branch<span class="token parameter"> -a</span></span>

<span class="token comment"># 新建一个分支，但依然停留在当前分支</span>
<span class="token command">$ git branch &lt;branch-name&gt;</span>

<span class="token comment"># 新建一个分支，并切换到该分支</span>
<span class="token command">$ git checkout<span class="token parameter"> -b</span> &lt;branch-name&gt;</span>

<span class="token comment"># 新建一个分支，指向指定commit</span>
<span class="token command">$ git branch &lt;branch-name&gt; [commit]</span>

<span class="token comment"># 新建一个分支，与指定的远程分支建立追踪关系</span>
<span class="token command">$ git branch<span class="token parameter"> --track</span> &lt;branch-name&gt; [remote-branch]</span>

<span class="token comment"># 切换到指定分支，并更新工作区</span>
<span class="token command">$ git checkout &lt;branch-name&gt;</span>

<span class="token comment"># 建立追踪关系，在现有分支与指定的远程分支之间</span>
<span class="token command">$ git branch<span class="token parameter"> --set</span>-upstream [branch-name] [remote-branch]</span>

<span class="token comment"># 合并指定分支到当前分支</span>
<span class="token command">$ git merge &lt;branch-name&gt;</span>

<span class="token comment"># 选择一个commit，合并进当前分支</span>
<span class="token command">$ git cherry-pick [commit]</span>

<span class="token comment"># 删除分支</span>
<span class="token command">$ git branch<span class="token parameter"> -d</span> &lt;branch-name&gt;</span>

<span class="token comment"># 强制删除一个分支，不管有没有未合并变化</span>
git branch -D &lt;branch-name&gt;

<span class="token comment"># 删除远程分支</span>
<span class="token command">$ git push origin<span class="token parameter"> --delete</span> &lt;branch-name&gt;</span>
<span class="token command">$ git branch<span class="token parameter"> -dr</span> &lt;remote/branch&gt;</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="分支改名" tabindex="-1"><a class="header-anchor" href="#分支改名" aria-hidden="true">#</a> 分支改名</h3><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code><span class="token comment"># 新建一个分支</span>
git checkout -b twitter-experiment feature132
<span class="token comment"># 删除原来的分支，使用新的分支，从而达到重命名操作</span>
git branch -d feature132
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code><span class="token comment"># 为当前分支改名</span>
git branch -m twitter-experiment

<span class="token comment"># 为指定分支改名</span>
git branch -m feature132 twitter-experiment

<span class="token comment"># 如果有重名分支，强制改名</span>
git branch -m feature132 twitter-experiment
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="查看-merge-情况" tabindex="-1"><a class="header-anchor" href="#查看-merge-情况" aria-hidden="true">#</a> 查看 merge 情况</h3><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code><span class="token comment"># 显示全部合并到当前分支的分支</span>
git branch --merged

<span class="token comment"># 显示未合并到当前分支的分支</span>
git branch --no-merged
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="git-cherry-pick" tabindex="-1"><a class="header-anchor" href="#git-cherry-pick" aria-hidden="true">#</a> git cherry-pick</h3><pre><code>git cherry-pick 命令”复制”一个提交节点并在当前分支做一次完全一样的新提交
</code></pre><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code><span class="token comment"># 选择一个commit，合并进当前分支</span>
<span class="token command">$ git cherry-pick [commit]</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="标签-tag" tabindex="-1"><a class="header-anchor" href="#标签-tag" aria-hidden="true">#</a> 标签(tag)</h3><pre><code>git tag 命令用于为 commit 打标签
Tag 分两种：普通 tag 和注解 tag
</code></pre><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code>git tag 1.0.0
git push --tags

git tag v0.0.1
git push origin master --tags

</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code><span class="token comment"># 列出所有tag</span>
git tag

<span class="token comment"># 新建一个tag在当前commit</span>
git tag [tag]

<span class="token comment"># 新建一个tag在指定commit</span>
git tag [tag] [commit]

<span class="token comment"># 查看tag信息</span>
git show [tag]

<span class="token comment"># 提交指定tag</span>
git push [remote] [tag]

<span class="token comment"># 提交所有tag</span>
git push [remote] --tags

<span class="token comment"># 新建一个分支，指向某个tag</span>
git checkout -b [branch] [tag]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="查看信息" tabindex="-1"><a class="header-anchor" href="#查看信息" aria-hidden="true">#</a> 查看信息</h3><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code><span class="token comment"># 显示有变更的文件</span>
git status

<span class="token comment"># 显示当前分支的版本历史</span>
git log

<span class="token comment"># 显示commit历史，以及每次commit发生变更的文件</span>
git log --stat

<span class="token comment"># 显示某个文件的版本历史，包括文件改名</span>
git log --follow [file]
git whatchanged [file]

<span class="token comment"># 显示指定文件相关的每一次diff</span>
git log -p [file]

<span class="token comment"># 显示指定文件是什么人在什么时间修改过</span>
git blame [file]

<span class="token comment"># 显示暂存区和工作区的差异</span>
git diff

<span class="token comment"># 显示暂存区和上一个commit的差异</span>
git diff --cached [&lt;file&gt;]

<span class="token comment"># 显示工作区与当前分支最新commit之间的差异</span>
git diff HEAD

<span class="token comment"># 显示两次提交之间的差异</span>
git diff [first-branch]...[second-branch]

<span class="token comment"># 显示某次提交的元数据和内容变化</span>
<span class="token command">$ git show [commit]</span>

<span class="token comment"># 显示某次提交发生变化的文件</span>
git show --name-only [commit]

<span class="token comment"># 显示某次提交时，某个文件的内容</span>
git show [commit]:[filename]

<span class="token comment"># 显示当前分支的最近几次提交</span>
git reflog
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="远程同步-git-remote" tabindex="-1"><a class="header-anchor" href="#远程同步-git-remote" aria-hidden="true">#</a> 远程同步(git remote)</h3><pre><code>你从远程仓库克隆时，实际上 Git 自动把本地的 master 分支和远程的 master 分支对应起来了，并且，远程仓库的默认名称是 origin git remote 查看远程库的信息.
</code></pre><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code><span class="token comment"># 下载远程仓库的所有变动</span>
git fetch [remote]

<span class="token comment"># 查看远程库详细信息</span>
<span class="token comment"># 显示可以抓取(fetch)和推送(push)的origin的地址；如果没有推送权限，就看不到push的地址</span>
git remote -v

<span class="token comment"># 显示某个远程仓库的信息</span>
git remote show [remote]

<span class="token comment"># 增加一个新的远程仓库，并命名</span>
git remote add [shortname] [url]

<span class="token comment"># 取回远程仓库的变化，并与本地分支合并</span>
git pull [remote] [branch]

<span class="token comment"># 上传本地指定分支到远程仓库</span>
git push [remote] [branch]

<span class="token comment"># 强行推送当前分支到远程仓库，即使有冲突</span>
git push [remote] --force

<span class="token comment"># 推送所有分支到远程仓库</span>
git push [remote] --all

</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="查看版本改动-git-diff" tabindex="-1"><a class="header-anchor" href="#查看版本改动-git-diff" aria-hidden="true">#</a> 查看版本改动(git diff)</h3><pre><code>git diff 命令用于查看文件之间的差异
在 git 提交环节，存在三大部分：working tree（工作区）, index file（暂存区：stage）, commit（分支：master）
</code></pre><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code><span class="token comment"># 查看工作区与暂存区的差异</span>
git diff

<span class="token comment"># 查看某个文件的工作区与暂存区的差异</span>
git diff [file.txt]

<span class="token comment"># 查看暂存区与当前 commit 的差异</span>
git diff --cached

<span class="token comment"># 查看两个commit的差异</span>
git diff &lt;commitBefore&gt; &lt;commitAfter&gt;

<span class="token comment"># 查看暂存区与仓库区的差异</span>
git diff --cached

<span class="token comment"># 查看工作区与上一次commit之间的差异</span>
<span class="token comment"># 即如果执行 git commit -a，将提交的文件</span>
git diff HEAD

<span class="token comment"># 查看工作区与某个 commit 的差异</span>
git diff &lt;commit&gt;

<span class="token comment"># 显示两次提交之间的差异</span>
git diff [first-branch]...[second-branch]

<span class="token comment"># 查看工作区与当前分支上一次提交的差异，但是局限于test文件</span>
git diff HEAD -- ./test

<span class="token comment"># 查看当前分支上一次提交与上上一次提交之间的差异</span>
git diff HEAD -- ./test

<span class="token comment"># 生成patch</span>
git format-patch master --stdout &gt; mypatch.patch
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>比较两个分支</p><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code><span class="token comment"># 查看topic分支与master分支最新提交之间的差异</span>
git diff topic master

<span class="token comment"># 与上一条命令相同</span>
git diff topic..master

<span class="token comment"># 查看自从topic分支建立以后，master分支发生的变化</span>
git diff topic...master
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="撤销" tabindex="-1"><a class="header-anchor" href="#撤销" aria-hidden="true">#</a> 撤销</h3><pre><code>::: tip 注意
HEAD 表示当前版本，也就是最新的提交。上一个版本就是 HEAD^，上上一个版本就是 HEAD^^，
往上 100 个版本写 100 个^ 比较容易数不过来，所以写成 HEAD~100。HEAD~2 相当于 HEAD^^
:::
</code></pre><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code><span class="token comment"># 恢复暂存区的指定文件到工作区</span>
git checkout [file]

<span class="token comment"># 恢复某个commit的指定文件到工作区</span>
git checkout [commit] [file]

<span class="token comment"># 恢复上一个commit的所有文件到工作区</span>
git checkout .

<span class="token comment"># 重置暂存区的指定文件，与上一次commit保持一致，但工作区不变</span>
git reset [file]

<span class="token comment"># 重置暂存区与工作区，与上一次commit保持一致</span>
git reset --hard

<span class="token comment"># 重置当前分支的指针为指定commit，同时重置暂存区，但工作区不变</span>
git reset [commit]

<span class="token comment"># 重置当前分支的HEAD为指定commit，同时重置暂存区和工作区，与指定commit一致</span>
git reset --hard [commit]

<span class="token comment"># 重置当前HEAD为指定commit，但保持暂存区和工作区不变</span>
git reset --keep [commit]

<span class="token comment"># 新建一个commit，用来撤销指定commit</span>
<span class="token comment"># 后者的所有变化都将被前者抵消，并且应用到当前分支</span>
git revert [commit]
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="其他" tabindex="-1"><a class="header-anchor" href="#其他" aria-hidden="true">#</a> 其他</h3><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code><span class="token comment"># 生成一个可供发布的压缩包</span>
git archive
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="安装" tabindex="-1"><a class="header-anchor" href="#安装" aria-hidden="true">#</a> 安装</h2>`,65),v={href:"https://git-scm.com/",target:"_blank",rel:"noopener noreferrer"},o=e(`<li><p>系统配置</p><ol><li>用户名与邮箱设置<div class="hint-container tip"><p class="hint-container-title">注意</p><p>输入用户名与邮箱作为标识，在桌面鼠标右键打开 Git Bash Here 命令，options 可以设置字体大小</p></div></li></ol><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code>设置用户名和邮箱
git config --global user.name <span class="token string">&quot;Your Name&quot;</span>
git config --global user.email <span class="token string">&quot;email@example.com&quot;</span>
查看设置的用户名与邮箱
git config --global user.name
git config --global user.email
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ol start="2"><li>SSH 公钥生成 接着上面的步骤，输入命令后按三次回车<div class="hint-container tip"><p class="hint-container-title">注意</p><p>邮箱要和上面设置的邮箱一致。 代表生成 SSH 公钥成功，在 C 盘&gt;用户&gt;admin 目录下会生成一个.ssh 文件</p></div></li></ol><div class="language-git line-numbers-mode" data-ext="git"><pre class="language-git"><code><span class="token comment"># 输入如下命令，三次回车即可生成 ssh key</span>
ssh-keygen -t rsa -C <span class="token string">&quot;email@example.com&quot;</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div></li><li><p>码云账号配置</p><ol><li>点击设置&gt;SSH 公钥</li><li>获取 SSH 公钥 打开文件，C&gt;用户&gt;admin&gt;.ssh，用记事本或者其他文本编辑器打开 id_ras.pub 文件</li></ol></li>`,2);function u(b,g){const s=l("ExternalLinkIcon");return d(),c("div",null,[r,n("ol",null,[n("li",null,[n("p",null,[n("a",v,[i("git 官网"),t(s)]),i("下载安装")])]),o])])}const k=a(m,[["render",u],["__file","git.html.vue"]]);export{k as default};
