import{_ as i}from"./plugin-vue_export-helper-x3n3nnut.js";import{o as l,c as e,e as t}from"./app-fFzGerwm.js";const d={},s=t(`<h2 id="css" tabindex="-1"><a class="header-anchor" href="#css" aria-hidden="true">#</a> css</h2><h2 id="属性值的计算过程" tabindex="-1"><a class="header-anchor" href="#属性值的计算过程" aria-hidden="true">#</a> 属性值的计算过程</h2><ul><li>initial:默认值</li><li>inherits:继承</li><li>unset:设置默认样式，清楚样式</li><li>revert:回归到浏览器的默认样式</li></ul><div class="language-text line-numbers-mode" data-ext="text"><pre class="language-text"><code>&lt;style&gt;
    &lt;!--
    清除默认样式

    --&gt;
    body，body *:not(script) {
      all: unset;
    }
  &lt;/style&gt;
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li>某个元素从所有CSS属性没有值，到所有css都有值的过程 <ul><li><p>确定声明值</p></li><li><p>层叠</p><ul><li><p>比较重要性</p><ol><li>带有important的作者样式</li><li>带有important的默认样式</li><li>作者样式</li><li>默认样式</li></ol></li><li><p>比较特殊性</p><ul><li>对每个样式分别计数</li></ul><table><thead><tr><th>style</th><th>id</th><th>属性</th><th>元素</th></tr></thead><tbody><tr><td>内联：0，否则：1</td><td>id选择器的数量</td><td>属性，类，伪类的数量</td><td>元素，微元素的数量</td></tr></tbody></table></li><li><p>比较源次序</p><ul><li>源码中靠后的元素覆盖靠前的元素</li></ul></li></ul></li><li><p>继承</p><ul><li>对目前仍然没有值==的属性，若可以继承的属性，则可以使用继承</li><li>可以继承的属性：文字相关</li></ul></li><li><p>使用默认值</p></li></ul></li></ul><h2 id="视觉格式化模型" tabindex="-1"><a class="header-anchor" href="#视觉格式化模型" aria-hidden="true">#</a> 视觉格式化模型</h2>`,6),a=[s];function n(r,c){return l(),e("div",null,a)}const o=i(d,[["render",n],["__file","css01.html.vue"]]);export{o as default};
