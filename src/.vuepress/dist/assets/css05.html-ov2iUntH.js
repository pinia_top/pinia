import{_ as n}from"./plugin-vue_export-helper-x3n3nnut.js";import{o as s,c as a,e as t}from"./app-fFzGerwm.js";const p={},e=t(`<h2 id="简版" tabindex="-1"><a class="header-anchor" href="#简版" aria-hidden="true">#</a> 简版</h2><p>css 画三角形的原理是利用盒子边框完成的,实现步骤可以分为以下四步:</p><ol><li>设置一个盒子</li><li>设置四周不同颜色的边框</li><li>将盒子宽高设置为 0，仅保留边框</li><li>4.得到四个三角形，选择其中一个后，其他三角形（边框）设置颜色为透明</li></ol><div class="language-css line-numbers-mode" data-ext="css"><pre class="language-css"><code><span class="token selector">div</span> <span class="token punctuation">{</span>
	<span class="token property">width</span><span class="token punctuation">:</span> 0px<span class="token punctuation">;</span>
	<span class="token property">height</span><span class="token punctuation">:</span> 0px<span class="token punctuation">;</span>
	<span class="token property">border-top</span><span class="token punctuation">:</span> 20px solid transparent<span class="token punctuation">;</span>
	<span class="token property">border-bottom</span><span class="token punctuation">:</span> 20px solid red<span class="token punctuation">;</span>
	<span class="token property">border-left</span><span class="token punctuation">:</span> 20px solid transparent<span class="token punctuation">;</span>
	<span class="token property">border-right</span><span class="token punctuation">:</span> 20px solid transparent<span class="token punctuation">;</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="应用场景" tabindex="-1"><a class="header-anchor" href="#应用场景" aria-hidden="true">#</a> 应用场景</h3><p>在前端开发的时候，我们有时候会需要用到一个三角形的形状，比如地址选择或者播放器里面播放按钮</p><p>通常情况下，我们会使用图片或者 svg 去完成三角形效果图，但如果单纯使用 css 如何完成一个三角形呢？</p><p>实现过程似乎也并不困难，通过边框就可完成</p><h3 id="空心三角行" tabindex="-1"><a class="header-anchor" href="#空心三角行" aria-hidden="true">#</a> 空心三角行</h3><p><code>思想：</code>如果想要实现一个只有边框是空心的三角形，由于这里不能再使用 border 属性，所以最直接的方法是利用伪类新建一个小一点的三角形定位上去</p><div class="language-css line-numbers-mode" data-ext="css"><pre class="language-css"><code><span class="token selector">.border</span> <span class="token punctuation">{</span>
	<span class="token property">width</span><span class="token punctuation">:</span> 0<span class="token punctuation">;</span>
	<span class="token property">height</span><span class="token punctuation">:</span> 0<span class="token punctuation">;</span>
	<span class="token property">border-style</span><span class="token punctuation">:</span> solid<span class="token punctuation">;</span>
	<span class="token property">border-width</span><span class="token punctuation">:</span> 0 50px 50px<span class="token punctuation">;</span>
	<span class="token property">border-color</span><span class="token punctuation">:</span> transparent transparent #d9534f<span class="token punctuation">;</span>
	<span class="token property">position</span><span class="token punctuation">:</span> relative<span class="token punctuation">;</span>
<span class="token punctuation">}</span>
<span class="token selector">.border:after</span> <span class="token punctuation">{</span>
	<span class="token property">content</span><span class="token punctuation">:</span> <span class="token string">&quot;&quot;</span><span class="token punctuation">;</span>
	<span class="token property">border-style</span><span class="token punctuation">:</span> solid<span class="token punctuation">;</span>
	<span class="token property">border-width</span><span class="token punctuation">:</span> 0 40px 40px<span class="token punctuation">;</span>
	<span class="token property">border-color</span><span class="token punctuation">:</span> transparent transparent #96ceb4<span class="token punctuation">;</span>
	<span class="token property">position</span><span class="token punctuation">:</span> absolute<span class="token punctuation">;</span>
	<span class="token property">top</span><span class="token punctuation">:</span> 6px<span class="token punctuation">;</span>
	<span class="token property">left</span><span class="token punctuation">:</span> -40px<span class="token punctuation">;</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,11),o=[e];function c(i,l){return s(),a("div",null,o)}const d=n(p,[["render",c],["__file","css05.html.vue"]]);export{d as default};
