import{_ as s}from"./plugin-vue_export-helper-x3n3nnut.js";import{o as n,c as a,e}from"./app-fFzGerwm.js";const i={},l=e(`<h2 id="简版" tabindex="-1"><a class="header-anchor" href="#简版" aria-hidden="true">#</a> 简版</h2><ul><li>css 选择器非常多,可以分为基础选择器 复合选择器,伪类选择器;伪元素选择器</li><li>基础选择器: id 选择器 ，class 类选择器，标签选择器，</li><li>复合选择器: 后代选择器，子代选择器，并集选择器,交集选择器</li></ul><h2 id="选择器" tabindex="-1"><a class="header-anchor" href="#选择器" aria-hidden="true">#</a> 选择器</h2><p>CSS 选择器用于选择要应用样式的 HTML 元素。</p><h3 id="常见的-css-选择器有" tabindex="-1"><a class="header-anchor" href="#常见的-css-选择器有" aria-hidden="true">#</a> 常见的 CSS 选择器有：</h3><ol><li>标签选择器：通过标签名选择元素。例如，p 选择器选择所有段落元素。</li></ol><div class="language-css line-numbers-mode" data-ext="css"><pre class="language-css"><code><span class="token selector">p</span> <span class="token punctuation">{</span>
	<span class="token comment">/* styles go here */</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ol start="2"><li>ID 选择器：通过元素的 ID 属性选择元素。ID 属性必须唯一。例如，#header 选择器选择具有 id=&quot;header&quot;的元素。</li></ol><div class="language-css line-numbers-mode" data-ext="css"><pre class="language-css"><code><span class="token selector">#header</span> <span class="token punctuation">{</span>
	<span class="token comment">/* styles go here */</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ol start="3"><li>类选择器：通过元素的 class 属性选择元素。类可以在多个元素中共享。例如，.button 选择器选择具有 class=&quot;button&quot;的元素。</li></ol><div class="language-css line-numbers-mode" data-ext="css"><pre class="language-css"><code><span class="token selector">.button</span> <span class="token punctuation">{</span>
	<span class="token comment">/* styles go here */</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ol start="4"><li>属性选择器：通过元素的属性选择元素。例如，[type=&quot;text&quot;]选择器选择具有 type=&quot;text&quot;属性的元素</li></ol><div class="language-css line-numbers-mode" data-ext="css"><pre class="language-css"><code><span class="token selector">[type=&quot;text&quot;]</span> <span class="token punctuation">{</span>
  <span class="token comment">/* styles go here */</span>
<span class="token punctuation">}</span>

[attribute] 选择带有attribute属性的元素
[attribute=value] 选择所有使用attribute=value的元素
[attribute~=value] 选择attribute属性包含value的元素
[attribute|=value]：选择attribute属性以value开头的元素
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ol start="5"><li>后代选择器：选择嵌套在其他元素内部的元素。例如，ul li 选择器选择 ul 元素内部的所有 li 元素。</li></ol><div class="language-css line-numbers-mode" data-ext="css"><pre class="language-css"><code><span class="token selector">ul li</span> <span class="token punctuation">{</span>
	<span class="token comment">/* styles go here */</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ol start="6"><li>子选择器：选择作为父元素直接子元素的元素。例如，ul &gt; li 选择器选择 ul 元素的直接子元素 li。</li></ol><div class="language-css line-numbers-mode" data-ext="css"><pre class="language-css"><code><span class="token selector">ul &gt; li</span> <span class="token punctuation">{</span>
	<span class="token comment">/* styles go here */</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ol start="7"><li>相邻同胞选择器（.one+.two），选择紧接在.one 之后的所有.two 元素</li></ol><div class="language-css line-numbers-mode" data-ext="css"><pre class="language-css"><code><span class="token selector">.one + .two</span> <span class="token punctuation">{</span>
	<span class="token comment">/* styles go here */</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ol start="8"><li>群组选择器（div,p），选择 div、p 的所有元素</li></ol><div class="language-css line-numbers-mode" data-ext="css"><pre class="language-css"><code><span class="token selector">div,
span</span> <span class="token punctuation">{</span>
	<span class="token comment">/* styles go here */</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="相对不常用的选择器" tabindex="-1"><a class="header-anchor" href="#相对不常用的选择器" aria-hidden="true">#</a> 相对不常用的选择器</h3><ol><li>伪类选择器</li></ol><div class="language-css line-numbers-mode" data-ext="css"><pre class="language-css"><code><span class="token punctuation">:</span>link <span class="token property">：选择未被访问的链接</span>
<span class="token punctuation">:</span><span class="token property">visited：选取已被访问的链接</span>
<span class="token punctuation">:</span><span class="token property">active：选择活动链接</span>
<span class="token punctuation">:</span>hover <span class="token property">：鼠标指针浮动在上面的元素</span>
<span class="token punctuation">:</span>focus <span class="token property">：选择具有焦点的</span>

<span class="token punctuation">:</span>first-of-type <span class="token property">表示一组同级元素中其类型的第一个元素</span>
<span class="token punctuation">:</span>last-of-type <span class="token property">表示一组同级元素中其类型的最后一个元素</span>

<span class="token punctuation">:</span>only-of-type <span class="token property">表示没有同类型兄弟元素的元素</span>
<span class="token punctuation">:</span>only-child <span class="token property">表示没有任何兄弟的元素</span>

<span class="token punctuation">:</span><span class="token function">nth-child</span><span class="token punctuation">(</span>n<span class="token punctuation">)</span> <span class="token property">根据元素在一组同级中的位置匹配元素</span>
<span class="token punctuation">:</span><span class="token function">nth-last-of-type</span><span class="token punctuation">(</span>n<span class="token punctuation">)</span> <span class="token property">匹配给定类型的元素，基于它们在一组兄弟元素中的位置，从末尾开始计数</span>

<span class="token punctuation">:</span><span class="token property">first-child：表示一组兄弟元素中的第一个元素</span>
<span class="token punctuation">:</span>last-child <span class="token property">表示一组兄弟元素中的最后一个元素</span>

<span class="token punctuation">:</span>root <span class="token property">设置HTML文档</span>
<span class="token punctuation">:</span>empty <span class="token property">指定空的元素</span>
<span class="token punctuation">:</span>enabled <span class="token property">选择可用元素</span>
<span class="token punctuation">:</span>disabled <span class="token property">选择被禁用元素</span>
<span class="token punctuation">:</span>checked <span class="token property">选择选中的元素</span>
<span class="token punctuation">:</span><span class="token function">not</span><span class="token punctuation">(</span>selector<span class="token punctuation">)</span> 选择与 &lt;selector&gt; 不匹配的所有元素
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ol start="2"><li>伪元素选择器</li></ol><div class="language-css line-numbers-mode" data-ext="css"><pre class="language-css"><code><span class="token punctuation">:</span><span class="token punctuation">:</span>first-letter <span class="token property">：用于选取指定选择器的首字母</span>
<span class="token punctuation">:</span><span class="token punctuation">:</span>first-line <span class="token property">：选取指定选择器的首行</span>
<span class="token punctuation">:</span><span class="token punctuation">:</span><span class="token property">before</span> <span class="token punctuation">:</span> <span class="token property">选择器在被选元素的内容前面插入内容</span>
<span class="token punctuation">:</span><span class="token punctuation">:</span><span class="token property">after</span> <span class="token punctuation">:</span> <span class="token property">选择器在被选元素的内容后面插入内容</span>
<span class="token punctuation">:</span><span class="token punctuation">:</span><span class="token property">selection</span> <span class="token punctuation">:</span> 选中用户选择的文本部分。
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><p>3.层次选择器</p><div class="language-css line-numbers-mode" data-ext="css"><pre class="language-css"><code><span class="token comment">/* h2 ~ p选择器选择h2元素之后的所有p元素。 */</span>
<span class="token selector">h2 ~ p</span> <span class="token punctuation">{</span>
	<span class="token comment">/* styles go here */</span>
<span class="token punctuation">}</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="优先级" tabindex="-1"><a class="header-anchor" href="#优先级" aria-hidden="true">#</a> 优先级</h2><p><code>!important &gt; 内联 &gt; ID选择器 &gt; 类选择器 &gt; 标签选择器 &gt; 通配符选择器 &gt; 属性继承</code></p><div class="hint-container tip"><p class="hint-container-title">注意</p><p>!important 声明，因为它可能会导致 CSS 样式难以维护和重构。优先级也应该根据具体情况进行调整和平衡，以确保样式的合理应用。</p></div><h2 id="继承属性" tabindex="-1"><a class="header-anchor" href="#继承属性" aria-hidden="true">#</a> 继承属性</h2><p>在 css 中，继承是指的是给父元素设置一些属性，后代元素会自动拥有这些属性</p><ol><li>字体系列属性</li></ol><div class="language-css line-numbers-mode" data-ext="css"><pre class="language-css"><code><span class="token property">font</span><span class="token punctuation">:</span>组合字体
<span class="token property">font-family</span><span class="token punctuation">:</span>规定元素的字体系列
<span class="token property">font-weight</span><span class="token punctuation">:</span>设置字体的粗细
<span class="token property">font-size</span><span class="token punctuation">:</span>设置字体的尺寸
<span class="token property">font-style</span><span class="token punctuation">:</span>定义字体的风格
<span class="token property">font-variant</span><span class="token punctuation">:</span>偏大或偏小的字体
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ol start="2"><li>文本系列属性</li></ol><div class="language-css line-numbers-mode" data-ext="css"><pre class="language-css"><code>text-indent：文本缩进
text-align：文本水平对刘
line-height：行高
word-spacing：增加或减少单词间的空白
letter-spacing：增加或减少字符间的空白
text-transform：控制文本大小写
direction：规定文本的书写方向
color：文本颜色
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ol start="3"><li>元素可见性</li></ol><div class="language-css line-numbers-mode" data-ext="css"><pre class="language-css"><code>visibility
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><ol start="4"><li>表格布局属性</li></ol><div class="language-css line-numbers-mode" data-ext="css"><pre class="language-css"><code>caption-side：定位表格标题位置
border-collapse：合并表格边框
border-spacing：设置相邻单元格的边框间的距离
empty-cells：单元格的边框的出现与消失
table-layout：表格的宽度由什么决定
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ol start="5"><li>列表属性</li></ol><div class="language-css line-numbers-mode" data-ext="css"><pre class="language-css"><code>list-style-type：文字前面的小点点样式
list-style-position：小点点位置
list-style：以上的属性可通过这属性集合
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ol start="6"><li>引用</li></ol><div class="language-css line-numbers-mode" data-ext="css"><pre class="language-css"><code>quotes：设置嵌套引用的引号类型
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><ol start="7"><li>cursor：箭头可以变成需要的形状</li></ol><div class="language-css line-numbers-mode" data-ext="css"><pre class="language-css"><code>cursor：箭头可以变成需要的形状
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><h2 id="无继承的属性" tabindex="-1"><a class="header-anchor" href="#无继承的属性" aria-hidden="true">#</a> 无继承的属性</h2><ol><li>display</li><li>文本属性：vertical-align、text-decoration</li><li>盒子模型的属性：宽度、高度、内外边距、边框等</li><li>背景属性：背景图片、颜色、位置等</li><li>定位属性：浮动、清除浮动、定位 position 等</li><li>生成内容属性：content、counter-reset、counter-increment</li><li>轮廓样式属性：outline-style、outline-width、outline-color、outline</li><li>页面样式属性：size、page-break-before、page-break-after</li></ol>`,49),t=[l];function c(p,o){return n(),a("div",null,t)}const u=s(i,[["render",c],["__file","css06.html.vue"]]);export{u as default};
