import { defineUserConfig } from "vuepress";
import theme from "./theme.ts";

export default defineUserConfig({
	base: "/pinia/",

	lang: "zh-CN",
	title: "林深不见鹿",
	description: "pinia的个人博客",

	theme,
});
