---
title: 06.兼容性管理
date: 2023-06-02
tag:
  - 面试题
---

## 简介

babel.config.js

## 官网

[babel 官网](https://babel.docschina.org/)

## 面试题

- 如何把 ES6 中的 class 转换为 ES5 的写法？
- 手写一个 JSX？
- ES7 中 async 和 await 如何实现？
