---
title: 02.模块化
date: 2023-06-02
---

## 简介

- 模块化是解决全局变量污染和依赖混乱的工具
- 模块化的出现吹响了前端崛起的号角
- 各种第三方库层出不穷的涌现，每个第三方库解决前端一部分问题

### 模块化

- 每个 js 文件都可以看成一个模块

```js
<script src = '123.js' type = 'module'>
```

- 每个模块可以有自己的内部实现，也可以导出某些功能供外部使用

```js
export default sum; //导出
import sum from "./sum.js"; //导入
```

## 标准

### 官方标准（ESModule）

### 社区标准（CommonJS，AMD，CMD，UMD）

##
