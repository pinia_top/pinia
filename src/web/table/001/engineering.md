---
title: 01.工程化
date: 2023-06-20
tag:
  - 面试题
---

## 简介

- 当项目上到一定规模，就需要工程化，划分模块，解决复杂的问题
- 问题
  - 全局污染问题
    - 随着 js 文件数量和代码量的增加，全局变量同名的几率将会陡然上升，开发人员不得不耗费大量的精力来规避这个问题
  - 依赖混问题
    - 如何规划 js 的引入顺序

## 工程化的学习路线

- 模块化
  - CommonJS
  - ES Module
  - AMD
  - CMD
  - UMD
- 包管理器 安装 升级 卸载 查询
  - npm
  - cnpm
  - pnpm
  - yarn
  - bower
- 构建工具

  - webpack
    - 概念
    - 原理
    - 配置
    - 扩展
    - 优化

- 脚手架
  - vite
  - vue-cli
  - umijs
  - create-react-app

## 面试题

### CommonJS 和 ES MOdule 的区别？
