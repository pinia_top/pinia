---
title: 05.ESLint代码风格管理器
date: 2023-06-02
---

## 官网

[官网](https://zh-hans.eslint.org/docs/latest/use/getting-started)

## 使用

项目中通过`.eslintrc`通过配置规则，来管理代码风格
