---
title: Node.js
date: 2023-04-30
isOriginal: true
category:
 - Node.js
tag:
  - nvm
  - node.js
  - yarn
---

## 简版

nvm 命令

```npm
nvm off                     // 禁用node.js版本管理(不卸载任何东西)
nvm on                      // 启用node.js版本管理
nvm install <version>       // 安装node.js的命名 version是版本号 例如：nvm install 16.8.0
nvm uninstall <version>     // 卸载node.js是的命令，卸载指定版本的nodejs，当安装失败时卸载使用
nvm ls                      // 显示所有安装的node.js版本
nvm list available          // 显示可以安装的所有node.js的版本
nvm use <version>           // 切换到使用指定的nodejs版本
nvm v                       // 显示nvm版本
nvm install stable          // 安装最新稳定版
```

## nvm（安装 nodejs 多版本切换）

1. 安装：

   1. [nvm](https://github.com/coreybutler/nvm-windows/releases)中拉到下方，建议下载 [nvm-setup.zip](https://link.csdn.net/?target=https%3A%2F%2Fgithub.com%2Fcoreybutler%2Fnvm-windows%2Freleases%2Fdownload%2F1.1.8%2Fnvm-setup.zip)。
      1. nvm-noinstall.zip：绿色免安装版，但使用时需进行配置。
      2. nvm-setup.zip：安装版，推荐使用这个。

2. 安装：

   1. 网上有的资料说是要先卸载已安装的 nodejs，其实不用，选择安装版，在安装的过程中，会检测到已安装的版本，提示是否管理，选择是即可。
   2. 安装就是下一步下一步，就不截图了，安装过程中需要选择 nvm 安装目录和 nodejs 的安装目录，不需要更改保持默认即可。

3. 安装确认

   打开 cmd，输入命令 nvm ，显示版本号和命令参数就表示安装成功。

4. 使用前的设置

   ::: info 注意
   使用前最好先设置镜像！
   否则安装 nodejs 的时候可能缺少 npm。
   找到刚才 nvm 的安装目录，有个 settings.txt，在里面添加上如下 2 行，这是设置了淘宝镜像。

   ```text
    node_mirror: https://npm.taobao.org/mirrors/node/
    npm_mirror: https://npm.taobao.org/mirrors/npm/
   ```

   :::

5. 使用

   1. cmd 中，输入命令 nvm list 可以查看当前电脑上的 node 版本
   2. 根据热心网友补充：cmd 中如果运行有问题（会提示权限之类的），可以使用管理员身份运行。

命令参考：

```npm
nvm off                     // 禁用node.js版本管理(不卸载任何东西)
nvm on                      // 启用node.js版本管理
nvm install <version>       // 安装node.js的命名 version是版本号 例如：nvm install 16.8.0
nvm uninstall <version>     // 卸载node.js是的命令，卸载指定版本的nodejs，当安装失败时卸载使用
nvm ls                      // 显示所有安装的node.js版本
nvm list available          // 显示可以安装的所有node.js的版本
nvm use <version>           // 切换到使用指定的nodejs版本
nvm v                       // 显示nvm版本
nvm install stable          // 安装最新稳定版
```

6. 可能存在问题

   1. 执行 vnm install 时 node 安装成功了，但是切换版本后使用时 npm 没成功，这是因为默认镜像没有下载安装 npm，执行第 4 步后，卸载对应版本重新安装一遍即可。
   2. 在 nvm 安装目录有 node 对应版本的目录，可以打开看看里面有没有 npm。

## nodejs 安装

1. 安装

   1. 在想安装的位置创建一个 Node 文件夹（不要有空格）
   2. 直接在官网下载进行安装

2. 环境配置

   1. 在 node.js 安装目录下新建两个文件夹 node_global 和 node_cache
   2. 在 C 盘找到 cmd.exe，以管理员身份运行

   ```npm
   npm config set prefix "D:\xx\nodejs\node_global"
   npm config set cache "D:\xx\nodejs\node_cache"
   ```

3. 配置环境变量：
   1. “环境变量” -> “系统变量”：新建一个变量名为 “NODE_PATH”， 值为“D:\xx\nodejs\node_modules\”
   2. “环境变量” -> “用户变量”：编辑用户变量里的 Path，将相应 npm 的路径（“C:\Users\用户名\AppData\Roaming\npm”）改为：“D:\xx\nodejs\node_global”

## yarn 安装及环境配置

1. 安装 yarn

   ```npm
   npm install -g yarn
   ```

2. 配置环境

   1. 在 node.js 安装目录下新建两个文件夹 yarn_global 和 yarn_cache

   ```npm
   yarn config set global-folder "D:\xx\nodejs\yarn_global"
   yarn config set cache-folder "D:\xx\nodejs\yarn_cache"
   ```

3. 查看相关路径，如果都在 D:\xx...，则正确

   1. 查看当前 npm 包的全局安装路径

   ```npm
   npm prefix -g
   ```

   2. 查看 yarn 全局 bin 位置

   ```npm
   yarn global bin
   ```

   3. 查看 yarn 全局安装位置

   ```npm
   yarn global dir
   ```

   4. 查看 yarn 全局 cache 位置

   ```npm
   yarn cache dir
   ```

4. 设置权限（文件夹点属性）
   将 node.js 里面的 node_global 和 node_cache，yarn_global 和 yarn_cache，node_modules，包括 node.js 自身，都设置为最高权限。
