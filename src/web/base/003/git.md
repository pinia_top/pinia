---
title: git的安装及其使用
date: 2023-04-30
pageview: true
isOriginal: true
---

## 简版

```git
ssh -T git@gitee.com或ssh -T git@github.com         //git切换gitee和github
git init                                            //初始化
git clone <https>                                   //clone一个git仓库
git add .                                           //将文件添加到缓存
git commit -m "第一次版本提交"                       //将缓存区内容添加到仓库中
git branch                                          //查看分支命令
git branch <branchname>                             //创建分支命令
git checkout <branchname>                           //切换分支命令
git branch -b <branchname>                          //创建并切换分支命令
git branch -d <branchname>                          //删除分支命令
git merge <branchname>                              //合并分支命令
git push -u -f origin master:<branchname>           //第一次提交项目到master分支    -f：强制推送    -u:选项指定一个默认主机     branchname:分支别名
git pull                                            //从远程获取最新版本并merge到本地
```

## 命令

### 将本地项目初始化

```git
# 初始化 在当前目录新建一个Git代码库
git init

# 新建一个目录，将其初始化为Git代码库
git init <project-name>

# 给项目添加远程仓库
git remote add origin git@github.xxx.git

# 本地分支和远程分支建立联系(使用git branch -vv  可以查看本地分支和远程分支的关联关系)
git branch --set-upstream-to=origin/远程分支 本地分支
```

### 查看

```git
# 查看工作目录和暂存区的状态
git status

# 查看工作区与暂存区的差异
git diff

# 查看提交历史
git log

# 查看命令历史
git reflog

# ------------------------分支------------------------

# 查看git本地分支
git branch

# 查看git远程分支
git branch -r

# 查看所有本地分支和远程分支，远程分支为红色
git branch -a

# 查看每个本地分支的最后一次提交
git branch -v

# 查看本地分支和远程分支联系
git branch -vv

# 切换git分支
git checkout <branch-name>

# 创建分支
git branch <branch-name>

# 新建并切换git分支
git checkout -b <branch-name>

# 删除git分支
git branch -d <branch-name>

# 强制删除git分支
git branch -D <branch-name>

# 删除git远程分支
git push origin -d <branch-name>

# 本地分支和远程分支建立联系
git branch --set-upstream-to=origin/远程分支 本地分支
# ------------------------分支------------------------

# 查看git 配置
git config -l

# 修改远程仓库地址
git remote set-url origin git@github.xxx.git

# 本地分支回滚到指定版本
git reset --hard <commit ID号>

# 强制推送到远程分支
git push -f origin <branch name>
```

### 删除 commit 记录

::: info 注意
何时需要删除 Git 提交的历史记录

1. 当历史记录中出现过密码等敏感信息在历史记录中, 需要删除历史记录时
1. 当项目因历史记录过多, 导致历史记录占用了大量内存时, 比如 Github 仓库个人总容量时 1GB 不够用时
1. 当你想要一个全新的项目的时候, 并且想保持项目代码不变
   :::

```git

# 创建全新的孤立分支 latest_branch
git checkout --orphan <latest_branch>

# 2. 暂存所有文件
git add -A

# 3. 提交所有文件的修改到latest_branch
git commit -am "del all history"

# 4. 删除原来的master分支
git branch -D master

# 5. 修改latest_branch分支名为master
git branch -m master

# 6. 强制更新远程服务器的master分支, 至此清空git所有历史
git push -f origin master
```

### 新建代码库

```git
# 下载一个项目和它的整个代码历史
git clone [url]

# 将alpha目录（必须是git代码仓库），克隆到delta目录
# bare参数表示delta目录只有仓库区，没有工作区和暂存区，即delta目录中就是.git目录的内容
git clone alpha delta --bare
```

### 仓库与 git 的配置

```git
# 显示当前的Git配置
git config --list

# 编辑Git配置文件
git config -e [--global]

# 设置提交代码时的用户信息
git config [--global] user.name "[name]"
git config [--global] user.email "[email address]"
```

### git config

git 的设置文件为.gitconfig,它可以在用户主目录下，也可以在项目的目录之下

```git
# 显示当前的Git配置
git config --list
git config -l

# 编辑Git配置文件
git config -e [--global]

# 设置提交代码时的用户信息
# 参数
# 1.系统级别：--system
# 2.用户全局：--global
# 3.单独一个项目：--local
git config --global user.name "xxxx" #用户名
git config --global user.email "xxxx@xxx.com" #邮箱
git config --global core.editor vim #编辑器

git config --global alias.st status #按这种方法，配置别名
```

### 增加/删除文件

```git
# 添加指定文件到暂存区
git add [file1] [file2] ...

# 添加指定目录到暂存区，包括子目录
git add [dir]

# 添加当前目录的所有文件到暂存区
git add .

# 删除工作区文件，并且将这次删除放入暂存区
git rm [file1] [file2] ...

# 停止追踪指定文件，但该文件会保留在工作区
git rm --cached [file]

# 改名文件，并且将这个改名放入暂存区
git mv [file-original] [file-renamed]
```

### git add

1.  `git add`命令用于将变化的文件，从工作区提交到暂存区。它的作用就是告诉 Git，下一次哪些变化需要保存到仓库区。
2.  用户可以使用`git status`命令查看目前的暂存区放置了哪些文件。

```git
# 添加指定文件到暂存区
$ git add [file1] [file2] ...

# 添加指定目录到暂存区，包括子目录
$ git add [dir]

# 添加当前目录的所有文件到暂存区
# 会把当前目录中所有有改动的文件（不包括.gitignore中要忽略的文件）都添加到git缓冲区以待提交
$ git add .

# 会把当前目录中所有有改动的文件（包括.gitignore中要忽略的文件）都添加到git缓冲区以待提交
$ git add *		#<不推荐>
```

3.  -u 参数表示只添加暂存区已有的文件（包括删除操作），但不添加新增的文件。

```git
git add -u
```

4.  -A 或者--all 参数表示追踪所有操作，包括新增、修改和删除`Git 2.0 版开始，-A参数成为默认，即git add .等同于git add -A`
5.  -f 参数表示强制添加某个文件，不管.gitignore 是否包含了这个文件。

```git
$ git add -f <fileName>
```

6.  -p 参数表示进入交互模式，指定哪些修改需要添加到暂存区。即使是同一个文件，也可以只提交部分变动。

```git
# 添加每个变化前，都会要求确认，对于同一个文件的多处变化，可以实现分次提交
$ git add -p
```

::: tip 注意
注意，Git 2.0 版以前，git add 默认不追踪删除操作。即在工作区删除一个文件后，git add 命令不会将这个变化提交到暂存区，导致这个文件继续存在于历史中。Git 2.0 改变了这个行为。
:::

### 代码提交(git commit)

git commit 命令用于将暂存区中的变化提交到仓库区。-m 参数用于指定 commit 信息，是必需的。如果省略-m 参数，git commit 会自动打开文本编辑器，要求输入。

1.  -m 参数用于添加提交说明,如果没有指定提交说明，运行 commit 会直接打开默认的文本编辑器，让用户撰写提交说明.
2.  -a 参数用于先将所有工作区的变动文件，提交到暂存区，再运行 git commit。用了-a 参数，就不用执行 git add .命令了.
3.  --allow-empty 参数用于没有提交信息的 commit.
4.  –amend 参数用于撤销上一次 commit，然后生成一个新的 commit.
5.  --fixup 参数的含义是，当前添加的 commit 是以前某一个 commit 的修正。以后执行互动式的 git rebase 的时候，这两个 commit 将会合并成一个。
6.  --squash 参数的作用与--fixup 类似，表示当前添加的 commit 应该与以前某一个 commit 合并成一个，以后执行互动式的 git rebase 的时候，这两个 commit 将会合并成一个。

```git
# 提交暂存区到仓库区
git commit -m [message]

# 提交暂存区的指定文件到仓库区
git commit [file1] [file2] ... -m [message]

# 提交工作区自上次commit之后的变化，直接到仓库区
git commit -a

# 提交时显示所有diff信息
git commit -v

# 用于没有提交信息的 commit
$ git commit --allow-empty

# 使用一次新的commit，替代上一次提交
# 如果代码没有任何新变化，则用来改写上一次commit的提交信息
git commit --amend -m [message]

# 重做上一次commit，并包括指定文件的新变化
git commit --amend <file1> <file2> ...

# 提交说明将自动生成，即在目标 commit 的提交说明的最前面，添加"fixup!"这个词
git commit --fixup <commit>

```

### 分支

新建一个分支，指向当前 commit。本质是在 refs/heads/目录中生成一个文件，文件名为分支名，内容为当前 commit 的哈希值

    ::: tip 注意
    注意：创建后，还是停留在原来分支，需要用 git checkout 切换到新建分支
    :::

```git
# 列出所有本地分支
$ git branch

# 列出所有远程分支
$ git branch -r

# 列出所有本地分支和远程分支
$ git branch -a

# 新建一个分支，但依然停留在当前分支
$ git branch <branch-name>

# 新建一个分支，并切换到该分支
$ git checkout -b <branch-name>

# 新建一个分支，指向指定commit
$ git branch <branch-name> [commit]

# 新建一个分支，与指定的远程分支建立追踪关系
$ git branch --track <branch-name> [remote-branch]

# 切换到指定分支，并更新工作区
$ git checkout <branch-name>

# 建立追踪关系，在现有分支与指定的远程分支之间
$ git branch --set-upstream [branch-name] [remote-branch]

# 合并指定分支到当前分支
$ git merge <branch-name>

# 选择一个commit，合并进当前分支
$ git cherry-pick [commit]

# 删除分支
$ git branch -d <branch-name>

# 强制删除一个分支，不管有没有未合并变化
git branch -D <branch-name>

# 删除远程分支
$ git push origin --delete <branch-name>
$ git branch -dr <remote/branch>
```

### 分支改名

```git
# 新建一个分支
git checkout -b twitter-experiment feature132
# 删除原来的分支，使用新的分支，从而达到重命名操作
git branch -d feature132
```

```git
# 为当前分支改名
git branch -m twitter-experiment

# 为指定分支改名
git branch -m feature132 twitter-experiment

# 如果有重名分支，强制改名
git branch -m feature132 twitter-experiment
```

### 查看 merge 情况

```git
# 显示全部合并到当前分支的分支
git branch --merged

# 显示未合并到当前分支的分支
git branch --no-merged
```

### git cherry-pick

    git cherry-pick 命令”复制”一个提交节点并在当前分支做一次完全一样的新提交

```git
# 选择一个commit，合并进当前分支
$ git cherry-pick [commit]
```

### 标签(tag)

    git tag 命令用于为 commit 打标签
    Tag 分两种：普通 tag 和注解 tag

```git
git tag 1.0.0
git push --tags

git tag v0.0.1
git push origin master --tags

```

```git
# 列出所有tag
git tag

# 新建一个tag在当前commit
git tag [tag]

# 新建一个tag在指定commit
git tag [tag] [commit]

# 查看tag信息
git show [tag]

# 提交指定tag
git push [remote] [tag]

# 提交所有tag
git push [remote] --tags

# 新建一个分支，指向某个tag
git checkout -b [branch] [tag]
```

### 查看信息

```git
# 显示有变更的文件
git status

# 显示当前分支的版本历史
git log

# 显示commit历史，以及每次commit发生变更的文件
git log --stat

# 显示某个文件的版本历史，包括文件改名
git log --follow [file]
git whatchanged [file]

# 显示指定文件相关的每一次diff
git log -p [file]

# 显示指定文件是什么人在什么时间修改过
git blame [file]

# 显示暂存区和工作区的差异
git diff

# 显示暂存区和上一个commit的差异
git diff --cached [<file>]

# 显示工作区与当前分支最新commit之间的差异
git diff HEAD

# 显示两次提交之间的差异
git diff [first-branch]...[second-branch]

# 显示某次提交的元数据和内容变化
$ git show [commit]

# 显示某次提交发生变化的文件
git show --name-only [commit]

# 显示某次提交时，某个文件的内容
git show [commit]:[filename]

# 显示当前分支的最近几次提交
git reflog
```

### 远程同步(git remote)

    你从远程仓库克隆时，实际上 Git 自动把本地的 master 分支和远程的 master 分支对应起来了，并且，远程仓库的默认名称是 origin git remote 查看远程库的信息.

```git
# 下载远程仓库的所有变动
git fetch [remote]

# 查看远程库详细信息
# 显示可以抓取(fetch)和推送(push)的origin的地址；如果没有推送权限，就看不到push的地址
git remote -v

# 显示某个远程仓库的信息
git remote show [remote]

# 增加一个新的远程仓库，并命名
git remote add [shortname] [url]

# 取回远程仓库的变化，并与本地分支合并
git pull [remote] [branch]

# 上传本地指定分支到远程仓库
git push [remote] [branch]

# 强行推送当前分支到远程仓库，即使有冲突
git push [remote] --force

# 推送所有分支到远程仓库
git push [remote] --all

```

### 查看版本改动(git diff)

    git diff 命令用于查看文件之间的差异
    在 git 提交环节，存在三大部分：working tree（工作区）, index file（暂存区：stage）, commit（分支：master）

```git
# 查看工作区与暂存区的差异
git diff

# 查看某个文件的工作区与暂存区的差异
git diff [file.txt]

# 查看暂存区与当前 commit 的差异
git diff --cached

# 查看两个commit的差异
git diff <commitBefore> <commitAfter>

# 查看暂存区与仓库区的差异
git diff --cached

# 查看工作区与上一次commit之间的差异
# 即如果执行 git commit -a，将提交的文件
git diff HEAD

# 查看工作区与某个 commit 的差异
git diff <commit>

# 显示两次提交之间的差异
git diff [first-branch]...[second-branch]

# 查看工作区与当前分支上一次提交的差异，但是局限于test文件
git diff HEAD -- ./test

# 查看当前分支上一次提交与上上一次提交之间的差异
git diff HEAD -- ./test

# 生成patch
git format-patch master --stdout > mypatch.patch
```

比较两个分支

```git
# 查看topic分支与master分支最新提交之间的差异
git diff topic master

# 与上一条命令相同
git diff topic..master

# 查看自从topic分支建立以后，master分支发生的变化
git diff topic...master
```

### 撤销

    ::: tip 注意
    HEAD 表示当前版本，也就是最新的提交。上一个版本就是 HEAD^，上上一个版本就是 HEAD^^，
    往上 100 个版本写 100 个^ 比较容易数不过来，所以写成 HEAD~100。HEAD~2 相当于 HEAD^^
    :::

```git
# 恢复暂存区的指定文件到工作区
git checkout [file]

# 恢复某个commit的指定文件到工作区
git checkout [commit] [file]

# 恢复上一个commit的所有文件到工作区
git checkout .

# 重置暂存区的指定文件，与上一次commit保持一致，但工作区不变
git reset [file]

# 重置暂存区与工作区，与上一次commit保持一致
git reset --hard

# 重置当前分支的指针为指定commit，同时重置暂存区，但工作区不变
git reset [commit]

# 重置当前分支的HEAD为指定commit，同时重置暂存区和工作区，与指定commit一致
git reset --hard [commit]

# 重置当前HEAD为指定commit，但保持暂存区和工作区不变
git reset --keep [commit]

# 新建一个commit，用来撤销指定commit
# 后者的所有变化都将被前者抵消，并且应用到当前分支
git revert [commit]
```

### 其他

```git
# 生成一个可供发布的压缩包
git archive
```

## 安装

1. [git 官网](https://git-scm.com/)下载安装
2. 系统配置

   1. 用户名与邮箱设置
      ::: tip 注意
      输入用户名与邮箱作为标识，在桌面鼠标右键打开 Git Bash Here 命令，options 可以设置字体大小
      :::

   ```git
   设置用户名和邮箱
   git config --global user.name "Your Name"
   git config --global user.email "email@example.com"
   查看设置的用户名与邮箱
   git config --global user.name
   git config --global user.email
   ```

   2. SSH 公钥生成
      接着上面的步骤，输入命令后按三次回车
      ::: tip 注意
      邮箱要和上面设置的邮箱一致。
      代表生成 SSH 公钥成功，在 C 盘>用户>admin 目录下会生成一个.ssh 文件
      :::

   ```git
   # 输入如下命令，三次回车即可生成 ssh key
   ssh-keygen -t rsa -C "email@example.com"
   ```

3. 码云账号配置
   1. 点击设置>SSH 公钥
   2. 获取 SSH 公钥
      打开文件，C>用户>admin>.ssh，用记事本或者其他文本编辑器打开 id_ras.pub 文件
