---
icon: info
cover: http://pic.pinia.top/src/blog/longni.jpg
tag:
  - 个人简历
sticky: 10
---

# 个人简历

## 基本信息

- 姓名：高路路
- 学历：本科
- 专业：软件工程专业
- 手机号码：17603404200
- 电子邮箱：6668682@email.com
- 个人博客：[Blog](https://piniatop.gitee.io/pinia/)

## 技能

1. 编程语言：JavaScript, HTML, CSS, TypeScript, ES6, Sass, Less
2. 前端框架：Vue.js,
3. 组件库：Element-Ui, vant, Ant Design, Bootstrap
4. 版本控制：Git, SVN
5. 网络请求：AJAX, axios
6. 工具：Webpack, Babel, ESLint
7. 开发工具：VScode，WebStorm
8. UI 工具：photoshop, 蓝湖, 慕客

## 自我评价

1. 热爱前端开发：对前端开发充满热情，不断学习和追求新技术，保持对行业的敏感性。
2. 执行力强：能够高效地组织和完成工作任务，善于解决问题并保持良好的工作效率。
3. 良好的团队合作精神：擅长与团队合作，善于沟通和协调，能够与不同背景的人合作共事，共同实现项目目标。
4. 强大的学习能力：具备快速学习新技术和工具的能力，不断追求进步并积极探索创新解决方案。
5. 注重用户体验：关注用户需求和体验，能够从用户角度思考问题，设计和开发出用户友好的产品和界面。
6. 解决问题的能力：善于分析和解决技术问题，能够快速定位并找到合适的解决方案。
7. 扎实的计算机科学基础：熟悉数据结构，计算机网络，计算机组成原理，操作系统，C 语言等计算机基础知识。
