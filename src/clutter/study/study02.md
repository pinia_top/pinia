---
title: 01-读TypeScript入门教程的感悟
date: 2023-07-08
isOriginal: true
category:
  - 读书感悟
tag:
  - 读书感悟
---

1. TypeScript 是 JavaScript 的一个超集，主要提供了==类型系统==和对 ==ES6== 的支持，它由 Microsoft 开发，第一个版本是2012年10月，目前代码开源于 GitHub 上。
2. ts和js都是弱类型
3. 
4. 类型推论

# 基础

1. 原始数据类型，null和undefined和void的区别，null和undefined是任意类型的子类型
2. 任意值：在任意值上访问属性，调用方法都是允许的，返回的值的类型也是任意值
3. 类型推论
4. 联合类型:使用联合类型时，只能访问联合类型里共有的属性和方法
5. 接口:接口首字母大写，有的编程语言建议接口前加`I`
   - 可选属性
   - 任意属性:一旦定义了任意属性，确定属性和可选属性都必须是它的子类型
   - 只读属性
6. 数组类型
   - number[]
   - 数组泛型`Array<number>`
   - 接口描述数组
   - 类数组:需要使用接口定义
   - any[]
7. 函数的类型
   - 接口定义函数的形状
   - 可选参数:必须放在参数的最后，有默认参数在时，就不受限
   - 默认参数
   - 剩余参数
   - 重载:为了能够清除表达需求，需要函数定义，最后一次函数实现
8. 类型断言
   - as Type： 尽量使用as
   - `<Type>值`
   - 双重断言`as any ad Foo` : 除非迫不得已，不得使用双重类型断言
9. 声明文件
   1. 新语法
      - declare var [name]  declare let[name] 全局变量 declare const [name] 全局常量，一般会报错
      - declare function 全局方法
      - declare class 全局对类
      - declare enum 全局枚举
      - declare namespace 全局对象 ，对象拥有很深的层级，需要用namespace表明生成属性的类型
      - interface						type 全局类型
      - export namespace 导出对象
      - export default 默认导出 function class inerface 写在导出语句的最前边
      - export =commonjs 导出模块 `import foo = required('foo').bar`
      - export as namespace UMD库
      - declare global 全局变量的UND库
      - declare module 模块插件
      - `///<refernce types='node' />` 三线指令  types是库的依赖，path是文件的依赖
   2. [floder name].d.ts
   3. 第三方声明文件 @types/vue
      - 第三方声明文件搜索官网https://www.typescriptlang.org/dt/search
   4. 书写声明文件
10. 内置对象
    1. ECMAScript的内置对象:boolean,error,date,regexp
    2. 用ts写node，安装`npm install @types/node --save-dev`