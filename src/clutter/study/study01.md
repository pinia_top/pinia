---
title: 01-推荐书籍
date: 2023-05-06
isOriginal: true
category:
  - 读书感悟
tag:
  - 读书感悟
---

## 推荐书籍
- 《HTML、CSS和JavaScript入门经典（第2版）_朱莉·梅洛尼》
- 《CSS 权威指南 第4版》
- 《CSS揭秘》
- 《HTTP权威指南》
- 《IP（第五版）_乌尼日其其格》
- 《JavaScript DOM编程艺术(第2版)（图灵图书）》
- 《JavaScript高级程序设计（第4版）（图灵图书）_马特·弗里斯比》
- 《JavaScript权威指南（第七版》
- 《Node与Express开发》
- 《你不知道的JavaScript（上中下三卷）》
- 《深入解析CSS_基思· J·格兰特》
- 《数据结构_邓俊辉》
- 《算法导论》
- 《算法图解》
- 《图解HTTP》
- 《我的第一本算法书_石田保辉 宫崎修一》
- 《学习JavaScript数据结构与算法（第3版）_[巴西] 洛伊安妮 • 格罗纳 [格罗纳, 洛伊安妮 •]》

书籍在[阿里云盘](https://www.aliyundrive.com/s/AAQcRaDnQt2)有需要自取