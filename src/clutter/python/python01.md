---
title: 01.Python基础知识（一）
date: 2023-05-09
isOriginal: true
category:
 - python
tag:
  - python
---

## python 的输入输出

### print ()函数

print()可以输出数字，字符串(单，双引号)， 含有运算符的表达式

将数据输出到显示器：

将数据输出到文件中:

::: tip 注意

- 所指定的盘符必须存在
- 使用 file = fp
- a+就是文件不存在就创建，存在就在文件内容后面追加

:::

```py
fp = open ('F:/text.txt','a+')
print('445566','556666',file=fp)
fp.close()
```

### 转义字符

| 说明 | 描述 |
| ---- | ---- |
|换行:| \n n-->newline(换行符)的首字母|
|水平制表符:| \t t-->table()占满重新开一个，占不满不重新开|
|退格: |\b b-->blank|
|回车: |\r r-->renter(回车)|
|反斜杠:|\\|
|边界:|\||
|原字符:|\R 不希望字符串中的转义字符起作用，就使用原字符。注意最后一个字符不能是\||

### 二进制与字符编码

8bit = 1byte 1024byte = 1kB 1024kB = 1MB 1024MB = 1GB
1024GB = 1TB

2 4 8 16 32 64 128 256 512 1024

二进制-->ASCII(256)-->GB2312(1980,7445)-->GBK(1995)-->GB18030(00)-->UniCode(2byte)-->UTF-8

二进制 0b 八进制 0o 十六进制 0x

二进制转 UTF-8：chr()
UTF-8 转二进制：ord()

### 标识符和保留字

keyword():保留字
['False', 'None', 'True', 'and', 'as', 'assert', 'async', 'await', 'break', 'class', 'continue', 'def', 'del', 'elif', 'else', 'except', 'finally', 'for', 'from', 'global', 'if', 'import', 'in', 'is', 'lambda', 'nonlocal', 'not', 'or', 'pass', 'raise', 'return', 'try', 'while', 'with', 'yield']

标识符：变量，函数，类，模块及其它对象的名字就是标识符

### 变量

变量：内存中一个带标签的盒子

- 标识：表示对象所存储的内存地址，使用内置函数 id(obj)来获取
- 类型：表示对象的数据类型，使用内置函数 type(obj)来获取
- 值：表示对象所存储的具体数据，使用 print(obj)可以直接将值进行打印出来

```pythopn
name = '123'
print(name)
print('标识',id(name))
print('类型',type(name))
print('值',name)
# 标识 2892740513904
# 类型 <class 'str'>
# 值 123
```

多次赋值之后，变量名会指向新的空间

### 数据类型

常用的数据类型：整数(int)，浮点(float)，布尔(bool)，字符串(str)

#### float

浮点数存在不精确性
使用浮点数进行计算时，可能会出现小数位数不确定的情况

- 导入 decimal 模块

```puthon
from decimal import Decimal
print(Decimal('1.356')+Decimal('2.654'))
```

#### bool

会进行隐式转换

#### str

- `‘`，`“`只能在一行展示

- `“”“`能在多行显示

### 数据类型转换

- str():将其他的类型转换成字符串类型
- int()：将其他类型转换成整数类型
  - str-->int:str 必须是数据串
  - float-->int:截取小数部分，
- float()：将其他类型转换成浮点数类型
  - 文字类无法转换成浮点类

### python 的注释

- 单行注释，以`#`开头，
- 多行注释，是一对`"""注释"""`
- 中文编码注释，在文件开头加上中文声明注释`#coding:编码`

### input()函数

#### 语法

```python
demo = input('请输入')
```

#### 类型：str

### python 的运算符

- 标准运算符：+，-，\*，/，//,
- 取余运算符：%，
- 幂运算符：\*\*，

#### //

- 一正一负向下取整

#### %

- 余数 = 被除数-除数\*商

### python 的赋值运算符

- 从右向左
- 链式赋值`a = b = c = 20`
- 参数赋值：+=，-=，\*=，/=，//=，%=，
- 系列解包赋值`a,b,c = 10,20,30` 应用场景：交换俩数

### python 的比较运算符

- <,>,<=,>=,==,!=
- ==：对象的 value 的比较
- is，is not：对象的标识 id 比较
- and，or，not， in，in not：对于布尔值之间的运算

```python
s = '1122334455'
print('11' in s)//true
print('66' in not s)//true
```

### python 的位运算符

- 将数据转换成二进制进行计算
  - 位与& 对应数位都是 1，结果数位才为 1，否则为 0
  - 位与| 对应数位都是 0，结果数位才为 ，否则为 1
  - 左移位运算符 高位溢出舍弃，低位补 0
  - 右移位运算符 低位溢出舍弃，高位补 0

### 运算符的优先级

\*_ --> _,/,//,% --> +,- --> <<,>> --> & -->| --> <,>,<=,>=,==,!= --> and -->or -->=

算数运算符-->位运算符-->比较运算符--> 布尔运算符-->赋值运算符

## python 的组织结构

### 程序的组织结构

1996 年，任何算法都是有顺序结构，选择结构，循环结构这三种基本结构组合而成

### 顺序结构

程序从上至下依次执行代码，直至代码结束！！！

### 对象的布尔值

- python 一切皆对象，所有对象都有一个布尔值
  - 获取对象的布尔值，使用内置函数`bool()`
- 一下对象的布尔值位 False
  - False
  - 数值 0
  - None
  - 空字符串 ''
  - 空列表 list[]
  - 空元组 tuple()或者()
  - 空字典 dict()
  - 空集合 set()

### 分支结构

#### 单分支结构

```py
if 条件表达式:
    条件执行体

money = 1000
s = int(input('请输入取款金额'))
if money >= s:
    money -= s
    print('余额为',money)
```

#### 双分支结构

```py
if 条件表达式:
    条件执行体
else：
	条件执行体

num = int(input('请输入'))
if num%2 == 0:
    print('2')
else:
    print('1')
```

#### 多分支结构

```py
if 条件表达式1:
    条件执行体
elif 条件表达式2：
	条件执行体
elif 条件表达式3:
	条件执行体
[else:]
	条件执行体

num = int(input('请输入'))
if num >= 90:
    print('90')
elif num < 90 and num >= 80:
    print('80')
elif num < 80 and num >= 70:
    print('70')
elif num <70 and num >= 60:
    print('60')
else:
    print('10~60')

num = int(input('请输入'))
if num >= 90:
    print('90')
elif 90> num >= 80:
    print('80')
elif 80 > num >= 70:
    print('70')
elif 70 > num >= 60:
    print('60')
else:
    print('10~60')

```

#### 嵌套 if

```py
if 条件表达式1:
    if 内层条件表达式:
    	内层条件执行体1
	else：
		内层条件执行体2
else：
	条件执行体

num = int(input('请输入'))
if num > 60:
    if num >80:
        print('80')
    else:
        print('60')
else:
    print('不及格')
```

#### 条件表达式

语法：

```py
x if 判断条件 else y
num1 = int(input('请输入num1'))
num2 = int(input('请输入num2'))
print((str(num1)+'>='+str(num2)) if num1 >= num2 else (str(num1)+'<'+str(num2)))
```

运算规则：如果判断条件的布尔值为 True，条件表达式的返回值为，否则条件表达式的返回值为 False

### pass 空语句

pass 语句什么都不做，只需要一个占位符，用在语法需要的地方

```py
num = int(input('请输入'))
if num >= 90:
    pass
elif 90> num >= 80:
    pass
else:
    print('10~60')
```

## python 的循环

### 内置函数 range()

- range()函数
  - 用于生成一个整数序列
  - 创建 range 对象的三种方式
    - range(stop):创建一个[0,stop)之间的整数序列，步长为 1
    - range(start,stop):创建一个[start,stop)之间的整数序列，步长为 1
    - range(start,stop,step):创建一个[start,stop)之间的整数序列，步长为 step
  - 返回值是一个迭代器对象
  - range 类型的优点：不管 range 对象表示的整数序列有多长，所有 range 对象占用的内存空间都是相同的，因为仅仅需要存储 start，stop，step，只有当用到 range 对象时，才会去计算序列中的相关元素
  - in 与 not in 判断整数序列中是否存在(不存在)指定的整数:返回 boolean

```py
s = range(10,20,3)
print(list(s))

print( 13 in s)
```

### while

::: info 问题

选择结构的 if 与循环结构 while 的区别

- if 是判断一次，条件为 True 执行一行
- while 是判断 N+1 次，条件为 True 执行 N 次

:::

```py
while 条件表达式:
    条件执行体（循环体）

i = 0
sum = 0
while i <= 100:
    if not bool(i%2):
        sum += i
    i += 1
print(sum)
```

### for -in

- in 表达从（字符串，序列等）中依次取值，又称为遍历
- for - in 遍历的对象必须是可迭代对象

```py
for 自定义变量 in 可迭代对象：
	循环体

i = 0
for i in range(1,10):
    print(i)

i = 0
for _ in range(10):
    print('112233')
```

- 循环体内不需要访问自定义变量，可以将自定义变量代替为下划线

### 流程控制语句 break

- 用于结束循环结构，通常与分支结构 if 一起使用

```py
oldpwd = int(input('密码为：'))
i = 0
while i < 3:
    newpwd = int(input('请输入密码'))
    if oldpwd == newpwd:
        break
    else:
        print('密码错误')

    i += 1
```

### 流程控制语句 continue

- 用于结束当前循环，进入下一次循环，通常与分支结构一起使用

```py
for i in range(1,51):
    if i % 5 != 0:
        continue
    print(i)
```

### else 语句

- while 和 for in 没有碰到 break 就会执行 else

```py
for i in range(3):
    pwd = input('请输入密码')
    if pwd == '888':
        print('输入正确')
        break
    else:
        print('输入错误，请重新输入')

else:
    print('三次密码均错误')
```

### 嵌套循环

```py
for i in range(1,10):
    for j in range(1,i+1):
        print(str(j)+'*'+str(i)+'='+ str(i*j),end='\t')
    print()
```

### 二重循环中的 break 和 continue 用于控制本层循环

## 列表 list

### 列表的创建与删除

#### 列表

- 变量可以存储一个元素，列表是一个大容器，可以存储 N 个变量，程序可以方便地对这些数据进行整体操作

- 列表在其他语言中相当于数组
- 特点
  - 列表元素按顺序有序排列
  - 索引映射唯一数据
  - 列表可以存储重复数据
  - 任意数据类型混存
  - 根据需要动态分配和回收内存

#### 列表的创建

- 使用中括号
- 使用 list()

### 列表的查询操作

- 获取列表中指定元素的索引

  - 如果列表中存在 N 个相同的元素，只返回相同元素中的第一个元素的索引`list.index('112233')`

  - 如果查询的元素不在列表中，会抛出 ValueError

  - 还可以在指定的 start 和 stop 之间进行查找`list.index('112233',start,stop)

- 获取列表中的单个元素

  - 正向索引从 0 到 N-1`list[0]`
  - 逆向索引从-N 到-1`list[-1]`
  - 指定索引不存在，抛出 indexError

- 获取列表中的多个元素

  - 语法：`列表名[start:stop:step]`
  - 切片的结果：拷贝原列表片段：新的列表片段
  - 切片的范围：[start,stop）
  - step 为正数：[:stop:step] [start::step] 从 start 开始往后计算切片
  - step 为负数：[:stop:step] [start::step] 从 start 开始往前计算切片

- 判断指定元素在列表中是否存在

  - 语法：元素 in 列表名 元素 not in 列表名

- 列表元素的遍历
  - 语法：for 可迭代变量 in 列表名：

### 列表元素的增删改查

- 增加操作

  - append():在列表的末尾添加一个元素
  - extend():在列表的末尾至少添加一个元素
  - insert():在列表的任意位置添加一个元素
  - 切片：在列表的任意位置添加至少一个元素

- ```py
  a = ['123',123,bool(0)]
  a[1:1:1] = [1,2,3]
  print(a)
  ```

- 删除操作

  - remove():一次删除一个元素，重复元素只删除第一个，元素不存在抛出 ValueError
  - pop():删除一个指定索引位置上的元素，指定索引不存在抛出 IndexError，不指定索引，删除队列中最后一个元素
  - 切片：一次至少删除一个一个元素，产生一个新的列表

  ```py
  a = ['123',123,bool(0)]
  a[1:2] = []
  print(a)
  ```

  - clear():清空列表
  - del:删除列表

- 修改操作

  - 为指定索引的元素赋值一个新值
  - 为指定的切片赋予一个新值

  ```py
  a = ['123',123,bool(0)]
  a[0] = '444'
  print(a)
  ```

### 列表元素的排序

- 常见的俩种方法：

  - 调用 sort()方法，列表中的所有元素默认按照从小到大的顺序进行排序，可以指定 reverse=True，进行降序排序，原列表发生改变

  ```py
  a = [ 30,40,50,60,70,30,55,66,22,13,90]
  a.sort(reverse=True)
  print(a)
  ```

  - 调用内置函数 sorted()，可以指定 reverse=True，进行降序排序，产生一个新的列表，原列表不发生改变

  ```py
  a = [30,40,50,60,70,30,55,66,22,13,90]
  b = sorted(a,reverse=True)
  print(b)
  ```

### 列表生成式

- 语法：[表示列表元素的表达式 for 自定义变量 in 可迭代对象]

::: tip 注意事项

表示列表元素的表达式中通产包含自定义变量

列表中的元素都有一定的规则

:::

```py
a = [i for i in range(1,10)]
print(a)
```

## 字典 dict

### 字典的定义

- python 内置的数据结构之一，与列表一样是一个可变的序列
- 以键值对的方式存储数据，字典是一个无序的序列
- 类似于其他编程语言的对象
- 字典的特点
  - 字典中所有元素都是一个键值对，key 不允许重复，value 可以重复
  - 字典中的元素是无序的
  - 字典中的 key 必须是不可变对象
  - 字典也可以根据需求动态地伸缩
  - 字典会浪费较大的内存，是一种使用空间换时间的数据结构

### 字典的原理

- 字典的实现原理与查字典类似，查字典是先根据部首或拼音查找对应的页码，python 中的字典是根据 key 查找 value 所在的位置

### 字典的创建与删除

- 常用的方式{}
- 使用内置函数 dict()

```py
a = {"name":'张三',"age":88}
print(a)
b = dict(name='张三',age=15)
print(b)
```

### 字典的查询操作

#### 字典元素的获取

- [] ：
- get()：

```py
a = {"name":'张三',"age":88}
print(a["name"])
b = dict(name='张三',age=15)
print(b.get('name'))
```

- []与 get()取值的区别
  - []如果字典中不存在指定的 key，会抛出 keyError 异常
  - get()方法取值，如果字典中不存在指定的 key，并不会抛出 keyError 而是返回 None，可以通过参数设置默认的 value，以便指定的 key 不存在时的返回

#### key 的判断

- in 指定的 key 在字典中存在时返回 True
- not in 指定的 key 在字典中不存在时返回 True

#### 获取字典视图的三个方法

- keys()：获取字典中所有的键
- values()：获取字典中所有的值
- items()：获取字典中所有的键值对

```py
a = {"name":'张三',"age":88,'sex':'男'}
k = a.keys()
v = a.values()
i = a.items()
print(k)	#dict_keys(['name', 'age', 'sex'])
print(v)	#dict_values(['张三', 88, '男'])
print(i)	#dict_items([('name', '张三'), ('age', 88), ('sex', '男')])
```

#### 字典元素的遍历

for item in 字典元素:

```py
a = {"name":'张三',"age":88,'sex':'男'}
for item in a:
    print(a.get(item))
```

### 字典元素的增删改查

- 新增操作

```py
a = {"name":'张三',"age":88}
a['sex'] = '男'
print(a)
```

- 删除操作

```py
a = {"name":'张三',"age":88}
del a['age']
print(a)
```

- 清空操作

```py
a = {"name":'张三',"age":88}
a.clear()
print(a)
```

### 字典生成式

- 内置函数 zip()
  - 语法：`{表示字典key的表达式：表示字典value的表达式 for 自定义表达式key的变量,自定义表达式value的变量 in zip(可迭代对象)}`
  - 常用于可迭代的对象作为参数，将对象中对应的元素打包成一个元组，然后返回由这些元组组成的列表

```py
a = ["name","age",'sex']
b = ['张三',88,'男']
c = {i.upper():v for i,v in zip(a,b)}
print(c)    #{'NAME': '张三', 'AGE': 88, 'SEX': '男'}
```

## 元组 tuple

### 元组的定义

- python 的内置数据结构之一，是一个不可变序列

::: info 面试题

不可变序列和可变序列

- 不可变序列：字符串，元组
  - 不可变序列：没有增删改查操作，对象地址发生改变
- 可变序列，列表，字典，集合
  - 可变序列：可以对序列指向增删改查操作，对象地址不会发生改变

为什么要将元组设计成不可变序列

- 在多任务环境下，同时操作对象时不需要加锁
- 因此，在程序中尽量使用不可变序列
- 注意事项：元组中存储的是对象的引用
  - 如果元组中对象本身是不可变对象，则不能再引用其他对象
  - 如果元组中的对象是可变对象，则可变对象的引用不允许改变，但数据可以改变

:::

### 元组的创建方式

- 直接使用()，()可省略不写,只有一个元素时，逗号不可省
- 使用内置函数 tuple()
- 只有一个元素的元组需要使用逗号和小括号

```py
a = (1,3,5,4,6)
print(type(a))
b = 1,
print(type(b))
c = tuple(('123',1213,234))
print(type(c))
```

### 元组的遍历

- 元组是可迭代对象，可以使用 for in 进行遍历

```py
t = (10,[20,30],40,'112233')
for item in t:
    print(item,type(item))
"""
10 <class 'int'>
[20, 30] <class 'list'>
40 <class 'int'>
112233 <class 'str'>
"""
```

## 集合 set

### 集合的定义

- python 语言提供的内置数据结构
- 与列表，字典一样都属于可变类型的序列
- 集合是没有 value 的字典
- 集合的特点
  - 集合中的元素不允许重复
  - 集合中的元素是无序的

### 集合的创建

- 直接使用{}
- 使用内置函数 set()

```py
s = {2,4,5,6,7,8,9,2}
print(s)
b = set(range(6))
print(b)
```

- 定义一个空集合

```py
s = set()
```

### 集合的相关操作

#### 判断操作

- in
- not in

```py
s = {2,4,5,6,7,8,9,2}
print(12 not in s) #False
print(1 in s) #True
```

#### 新增操作

- add():一次添加一个元素
- update():一次至少添加一个元素

```py
s = {2,4,5,6,7,8,9,2}
s.add(123)
s.update({3,8,9,22,13})
print(s)
```

#### 删除操作

- remove():一次删除一个指定元素，如果指定元素不存在，抛出错误 KeyError
- discard():一次删除一个指定的元素，如果指定的元素不存在不抛出异常
- pop():一次只删除一个任意元素
- clear():清空集合

```py
s = {2,4,5,6,7,8,9,2}
s.remove(2)
s.discard(3)
y = s.pop()
print(s,y)
s.clear()
```

### 集合之间的关系

- 俩个集合是否相等
  - 可以使用运算符`==`或者`!=`进行判断
- 一个集合是否是另一个集合的子集
  - 可以调用的方法 issubset 进行判断
- 一个集合是否是另一个集合的超集
  - 可以调用的方法 issuperset 进行判断
- 俩个集合是否没有交集
  - 可以调用方法 isdisjoint 进行判断

```py
x = {1,2,3,4,5}
y = {2,3,4,5,1}
print(x==y)
print(x.issubset(y))
print(x.issuperset(y))
print(x.isdisjoint(y))
```

### 集合的数学操作

- 交集` intersection()`或`&`
- 并集 `union()`或`|`
- 差集`difference()`或`-`
- 对称差集`symmetric_difference()`或`^`

```py
x = {1,2,3,4,5}
y = {2,3,5}
print(x.intersection(y))
print(x.union(y))
print(x.difference(y))
print(x.symmetric_difference(y))
```

### 集合生成式

- 语法`{表示集合元素的表达式 for 自定义变量 in 可迭代对象 }`

```py
demo = { i*i for i in range(10)}
print(demo)
```

## 字符串

### 字符串的定义

- 在 python 中字符串是基本数据类型，是一个不可变的字符序列
- 即使字符串更改之后值一样，但地址也不一样

### 字符串的驻留机制

- 仅保存一份相同的切不可变的字符串方法，不同的值被存放在字符串的驻留池中，python 的驻留机制对相同的字符串只保留一份拷贝，后续创建相同的字符串时，不会开辟新的空间，而是把该字符串的地址赋给新创建的变量
- 驻留机制的几种情况（交互模式）
  - 字符串的长度为 0 或 1 时
  - 符合标识符的字符串(数字，字母，下划线 )
  - 字符串只在编译时进行驻留，而非运行时，只要调用了方法就是运行时
  - [-5,256]之间的整数数字
- sys 中 intern 方法强制 2 个字符串指向同一个对象
- PyCharm 对字符串进行了优化处理
- 驻留机制的优缺点
  - 当需要值相同的字符串时，可以直接从字符串池里拿出来使用，避免频繁的创建和销毁，提升效率和节约内存空间，因此拼接字符串和修改字符串时比较耗费性能的。
  - 在需要进行字符串拼接时建议使用 str 类型的 join 方法，而非+，因为 join()方法是先计算出字符串中的长度，然后拷贝，只 new 一次对象，效率要比“
    +”效率高。

### 字符串的常用操作

#### 查找操作

- index()：查找子串 sunstr 第一次出现的位置，如果查找的子串不存在时，则抛出 ValueError
- rindex()：查找子串 sunstr 最后一次出现的位置，如果查找的子串不存在时，则抛出 ValueError
- find()：查找子串 sunstr 第一次出现的位置，如果查找的子串不存在时，则返回-1
- refind()：查找子串 sunstr 最后一次出现的位置，如果查找的子串不存在时，则返回-1

```py
demo = 'Hello,world'
print(demo.index('l')) #找不到抛出substring not found
print(demo.rindex('l'))
print(demo.find('l')) # 找不到返回-1
print(demo.find('l'))
```

#### 大小写操作

- upper()：把字符串中所有的字符都转换成大写
- lower()：把字符串中所有的字符都转换成小写写
- swapcase()：把字符串中所有的大写字母转小写字母，把所有的小写字母转大写字母
- capitalize()：把第一个字符转换成大写，其余的偶转换成小写字母
- title()：把每个单词的第一个字符转换成大写，把每个单词的剩余字符都转换成小写

```py
demo = 'Hello,world'
print(demo.upper())
print(demo.lower())
print(demo.swapcase())
print(demo.capitalize())
print(demo.title())
```

#### 内容对齐操作

- center():居中对齐，第一个参数指定宽度，第二个参数指定填充符（可选，默认是空格），如果设置宽度小于实际宽度则返回原字符串
- ljust():左对齐，第一个参数指定宽度，第二个参数指定填充符（可选，默认是空格），如果设置宽度小于实际宽度则返回原字符串
- rjust():右对齐，第一个参数指定宽度，第二个参数指定填充符（可选，默认是空格），如果设置宽度小于实际宽度则返回原字符串
- zfill():右对齐，左边用 0 填充，该方法只接收一个参数，用于指定字符串的宽度，如果设置宽度小于实际宽度则返回原字符串（-号之后，负号也算数）

```py
demo = 'hello'
print(demo.center(10,'*'))
print(demo.ljust(10,'*'))
print(demo.rjust(10,'*'))
print(demo.zfill(10))
```

#### 字符串的分割

- split():
  - 从字符串的左边开始分割，默认的分割符是空格字符串，返回值是一个列表
  - 以通过参数 sep 指定分割字符串是分割符
  - 通过参数 maxsplit 指定分割字符串的最大分割次数，在经过最大分割次数之后，剩余的串会单独作为一部分
- rsplit():
  - 从字符串的右边开始分割，默认的分割符是空格字符串，返回值是一个列表
  - 以通过参数 sep 指定分割字符串是分割符
  - 通过参数 maxsplit 指定分割字符串的最大分割次数，在经过最大分割次数之后，剩余的串会单独作为一部分

```py
demo = 'hellohellohellohellohellohellohellohellohellohello'
print(demo.split(sep='h',maxsplit=5))
print(demo.rsplit(sep='ll',maxsplit=8))
```

#### 字符串的判断方法

- isidentifier():判断指定的字符串是不是合法字符
- isspace():判断指定的字符串是否全部由空白字符串组成（回车，换行，水平制表符）
- isalpha():判断指定的字符串是否全部由字母组成
- isdecimal():判断指定的字符串是否由十进制的数字组成
- isnumeric():判断指定的字符串是否全部数字组成
- isalnum():判断指定字符串是否全部由字母和数字组成

```py
demo = 'hellohellohellohellohellohellohellohellohellohello'
print(demo.isidentifier())
print(demo.isspace())
print(demo.isalpha())
print(demo.isdecimal())
print(demo.isnumeric())
print(demo.isalnum())
```

#### 字符串的替换和合并

- repalce():第一个参数指定被替换的子串，第二个参数指定替换字符串的字符串，第三个参数是指定最大的替换次数，该方法返回替换后的字符串，替换前的字符串不发生改变，
- join():将列表或元组中的元素合并成一个字符串

```py
demo = 'hellohellohellohellohellohellohellohellohellohello'
print(demo.replace('ll','',5))
demo = ('1','2','3')
demo = ['1','2','3']
print(''.join(demo))
print('*'.join('gaolulu')) #g*a*o*l*u*l*u
```

#### 字符串的比较操作

- 比较运算符：> < >= <= == !=
- 比较规则:首先比较俩个字符串中的第一个字符，如果相等则比较下一个字符串，依次比较下去，直至出现不相等的字符时，其比较结果就是俩个字符串的比较结果，俩个字符串后续的字符将不再比较
- 比较原理：俩个字符比较时，比较的是其 ordinal value(原始值),调用内置函数 ord 可以得到指定字符的 ordinal value。与内置函数 ord 对应的内置函数 chr，调用内置函数 chr 时指定 ordinal value 可以得到其对应的字符

```py
print('apple'>'app') #True
print(ord('a'),chr(89)) #97 Y
```

::: info 面试题

== 和 is 的区别？

- ==比较的是 value
- is 比较的是 id

:::

#### 字符串的切片操作

- 字符串时不可变类型
  - 不具备增，删，改，查操作
  - 切片操作产生新的对象

```py
demo = 'hello,world'
demo1 = '11111'
demo2 = '!'
demo3 = demo + demo2 +demo1
print(demo[:6]) #hello,
print(demo[6:]) #world
print(demo[2:8]) #llo,wo
print(demo3) # hello,world!11111
print(demo3[2:8:2])#low
```

#### 格式化字符串

- 格式化字符串的俩种方式
  - %作为占位符
    - 语法：'{name}xxxxxxxxx{age}'%(name,age)
    - %s：字符串
    - %i 或%d：整数 #%10i，10 表示的是宽度
    - %f：浮点数 #%10.3f，同时表示宽度和精度，数字表示宽度，.数字表示精度
  - {}作为占位符
    - 语法：’{0:10.3f}xxxxxxxx{1}‘.format(name,age) #0 代表占位符，10 表示宽度， :.3f 表示精度，:.3 表示位数
  - s-string
    - 语法：'{name}xxxxxxxx{age}'

#### 字符串的编码转换

- 编码：将字符串转换成二进制数据（bytes）
- 解码：将 bytes 类型的数据转换成字符串类型

```py
s = '这是一个示例'
x = s.encode(encoding='GBK')
y = s.encode(encoding='UTF-8')
print(x) #GBK是一个编码格式，一个中文占2个byte #b'\xd5\xe2\xca\xc7\xd2\xbb\xb8\xf6\xca\xbe\xc0\xfd'
print(y) #UTF-8是一个编码格式，一个中文占3个byte #b'\xe8\xbf\x99\xe6\x98\xaf\xe4\xb8\x80\xe4\xb8\xaa\xe7\xa4\xba\xe4\xbe\x8b'
print(x.decode(encoding='GBK')) #这是一个示例
print(y.decode(encoding='UTF-8')) #这是一个示例
```

## 函数

### 函数的创建与调用

#### 函数

- 函数就是执行特定任务和完成一段特定功能的一段代码
- 函数的优点
  - 复用代码
  - 隐藏实现细节
  - 提高可维护性
  - 提高可读性便于调试

#### 函数的创建

```py
def 函数名([输入参数])：
	函数体
    [return xxx]
```

#### 函数的调用

```py
函数名([实际参数])
```

### 函数的参数传递

- 位置实参
  - 根据形参对应的位置进行实参的传递
- 关键字实参
  - 根据形参名称进行实参的传递

```py
def calc(a,b): #形参
    return a+b

print(calc(1,3)) #实参	位置实参
print(calc(a=3,b=6)) #实参	关键字实参
```

- 在函数调用的过程中，进行实参传递
  - 如果是不可变对象，在函数体的修改不会影响实参的值
  - 如果是可变对象，在函数体的修改会影响到实参的值

### 函数的返回值

- 返回值为一个值时，直接返回值
- 返回值返回多个值时，结果为元组
- 如果函数没有返回值，return 可以省略不写
  - 函数是否需要返回值，视情况而定

### 函数的参数定义

- 函数定义默认值参数
  - 函数定义时，给形参设置默认值，只有与默认值不符的时候才需要传递实参

```py
def adb(a=10,b=200):
    print(a+b)
adb(1)
```

- 个数可变的位置参数
  - 定义参数时，可能无法事先确定传递的位置实参的个数时，使用可变的位置参数
  - 使用\*定义个数可变的位置形参
  - 结果为一个元组
- 个数可变的关键字形参
  - 定义函数时，无法事先确定传递的关键字实参的个数时，使用可变的关键字参参
  - 使用\*\*定义个数可变的关键字形参
  - 结果为一个字典

```py
def abd(*args):
    print(args)
abd(1,2,3,4,5,6,7,8,9,0) #(1, 2, 3, 4, 5, 6, 7, 8, 9, 0)

def acd(**kwargs):
    print(kwargs)
acd(a=1,b=2,c=3,d=4,e=5,f=6,g=7,h=8,i=9,j=0) #{'a': 1, 'b': 2, 'c': 3, 'd': 4, 'e': 5, 'f': 6, 'g': 7, 'h': 8, 'i': 9, 'j': 0}
```

- 位置可变的位置参数或者关键字参数只能有一个位置参数或者关键字参数
- 函数调用是可以用位置参数，或者关键字参数将实参中的每个元素转换为位置参数或者关键字参数
- _关键字形参：从这个 _ 之后的参数在函数调用时，只能使用关键字参数传递

```py
def func(a,b,c,*,d,e):
    print(a,b,c,d,e)
func(10,20,30,d=40,e=50)
```

- 函数定义时形参的顺序问题

```py
def func(a,b,c,*,d,e,**kwargs):
    pass
def func1(*args,**kwargs):
    pass
def func2(a,b,c=0,*args,d,**kwargs):
    pass
```

### 变量的作用域

- 程序代码可以访问该变量的区域
- 根据变量的可用范围可以分为局部变量和全局变量
  - 局部变量
    - 在函数内定义并使用的变量，只有在函数内部生效，局部变量使用 global 声明，这个变量就会成为全局变量
  - 全局变量
    - 函数体外定义的变量，可作用于函数内外

```py
def func(name):
    print(f"hello {name}")
func("pinia")
def abc(a,b):
    global c
    c=a+b
    print(c)
abc(1,2)
```

### 递归函数

- 如果在一个函数的函数体内调用了该函数体本身，这个函数就称为递归函数
- 递归的组成部分
  - 递归调用与递归终止条件
- 递归的调用过程
  - 每递归调用一次，都会在栈内分配一个栈帧
  - 每次执行一次函数，都会释放相应的空间
- 递归的优缺点
  - 缺点：占用内存多，效率地下
  - 优点：思路和代码简单

```py
def fac(n):
    if n == 1:
        return 1
    else:
        return n * fac(n - 1)

print(fac(5))

def fac(n):
    return 1 if n < 2 else n * fac(n - 1)
print(fac(5))
```

### 斐波那契数列

```py
def fib(n):
    return 1 if n < 3 else fib(n - 1) + fib(n - 2)
for i in range(7):
    print(fib(i), end=' ') #1 1 1 2 3 5 8
```
