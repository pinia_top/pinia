---
title: 01.TailWindCss的使用
date: 2023-06-13
isOriginal: true
category:
 - css
tag:
  - css
---

## 官网

[Tailwindcss](https://www.tailwindcss.cn/)

## 安装

- 安装 Tailwind CSS

::: code-tabs#shell

@tab npm

```sh
npm install -D tailwindcss
```

@tab yarn

```sh
yarn add tailwindcss --save-dev
```

:::

- 配置模板文件的路径

  在 `tailwind.config.js` 配置文件中添加所有模板文件的路径。

  ```js
  /** @type {import('tailwindcss').Config} */
  module.exports = {
    content: ["./src/**/*.{html,js}"],
    theme: {
      extend: {},
    },
    plugins: [],
  }
  ```

- 将加载 Tailwind 的指令添加到你的 CSS 文件中

  在你的主 CSS 文件中通过 `@tailwind` 指令添加每一个 Tailwind 功能模块。

  ```css
  @tailwind base;
  @tailwind components;
  @tailwind utilities;
  ```

- 开启 Tailwind CLI 构建流程

  运行命令行（CLI）工具扫描模板文件中的所有出现的 CSS 类（class）并编译 CSS 代码。

  ```ch
  npx tailwindcss -i ./src/input.css -o ./dist/output.css --watch
  ```

- 在 `<head>` 标签内引入编译好的 CSS 文件，然后就可以开始使用 Tailwind 的工具类 来为你的内容设置样式了。

