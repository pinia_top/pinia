---
title: 03.sass中按钮颜色过渡（颜色函数）
date: 2023-06-07
---

::: tabs#fruit

@tab npm

```sh
npm install sass
```

@tab yarn

```sh
yarn add sass
```

:::

::: vue-playground sass中的颜色函数

@file color.vue

```vue
<script setup>

</script>
<template>
  <div class="container">
    <button disabled class="btn type-1">按钮</button>
    <button class="btn type-2">按钮</button>
    <button class="btn type-3">按钮</button>
    <button class="btn type-4">按钮</button>
    <button disabled class="btn type-5">按钮</button>
  </div>
</template>
.container {
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100vh;
  @for $i from 1 through length($btn-color) {
    .btn.type-#{$i} {
      padding: 10px 20px;
      margin: 10px;
      border-radius: 5px;
      background-color: nth($btn-color, $i);
      color: #fff;

      &:hover {
        background-color: lighten(nth($btn-color, $i), 10%);
      }

      &:active {
        background-color: darken(nth($btn-color, $i), 20%);
      }

      &:disabled {
        background-color: lighten(nth($btn-color, $i), 20%);
      }
    }
  }
}
</style>
```

:::

::: details Code

```vue
<script setup>
import HelloWorld from './components/HelloWorld.vue'
import TheWelcome from './components/TheWelcome.vue'
</script>

<template>
  <div class="container">
    <button disabled class="btn type-1">按钮</button>
    <button class="btn type-2">按钮</button>
    <button class="btn type-3">按钮</button>
    <button class="btn type-4">按钮</button>
    <button disabled class="btn type-5">按钮</button>
  </div>
</template>

<style scoped lang="scss">
//@use "sass:color";
//五个按钮的颜色
$btn-color: #409eff, #67c23a, #e39827, #f54343, #6c6d71;

.container {
  display: flex;
  align-items: center;
  justify-content: center;
  height: 100vh;
  @for $i from 1 through length($btn-color) {
    .btn.type-#{$i} {
      padding: 10px 20px;
      margin: 10px;
      border-radius: 5px;
      background-color: nth($btn-color, $i);
      color: #fff;

      &:hover {
        background-color: lighten(nth($btn-color, $i), 10%);
      }

      &:active {
        background-color: darken(nth($btn-color, $i), 20%);
      }

      &:disabled {
        background-color: lighten(nth($btn-color, $i), 20%);
      }
    }
  }
}
</style>
```

:::