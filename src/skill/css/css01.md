---
title: 01.CSS属性计算的总流程
date: 2023-05-06
---
## css

## 属性值的计算过程

- initial:默认值
- inherits:继承
- unset:设置默认样式，清楚样式
- revert:回归到浏览器的默认样式

```
<style>
    <!--
    清除默认样式

    -->
    body，body *:not(script) {
      all: unset;
    }
  </style>
```



- 某个元素从所有CSS属性没有值，到所有css都有值的过程
  - 确定声明值
  
  - 层叠
    - 比较重要性
      1. 带有important的作者样式
      2. 带有important的默认样式
      3. 作者样式
      4. 默认样式
      
    - 比较特殊性
    
      - 对每个样式分别计数
    
      | style            | id             | 属性                 | 元素               |
      | ---------------- | -------------- | -------------------- | ------------------ |
      | 内联：0，否则：1 | id选择器的数量 | 属性，类，伪类的数量 | 元素，微元素的数量 |
    
    - 比较源次序
    
      - 源码中靠后的元素覆盖靠前的元素
    
  - 继承
  
    - 对目前仍然没有值==的属性，若可以继承的属性，则可以使用继承
    - 可以继承的属性：文字相关
  
  - 使用默认值

## 视觉格式化模型

