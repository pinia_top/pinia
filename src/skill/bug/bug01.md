---
title: 01.setup关于props的报错
date: 2023-06-11
---

## Bug

```js
ESLint: Getting a value from the `props` in root scope of `<script setup>` will cause the value to lose reactivity.(vue/no-setup-props-destructure)
```

## 解决方案

- 在你缩写 defineProps 的代码上加注释

```js
/* eslint-disable */
// eslint-disable-next-line vue/no-setup-props-destructure
```

