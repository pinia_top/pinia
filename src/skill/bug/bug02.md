---
title: 02.Vue3的props传值被消除响应式
date: 2023-06-11
---

### 子组件接收父组件的传参。

- 父组件：

```vue
<template>
	<Son :data="data"/>
</template>
<script setup>
import { ref } from "vue";
let data = ref('hello')
setTimeout(() => {
	data.value = 'how are you doing'
}, 2000)

</script>
```

- 子组件：

```vue
<template>
	<div>{{ msg }}</div>
</template>
<script setup>
import { ref } from "vue";
// 接受来自父组件的传参
const props = defineProps({
  data: String,
});
const msg = ref(props.data);

</script>

```

此时父组件改变 data 的值，子组件 msg 无法响应 data 的变化。
因为 ref 是对传入数据的拷贝，原始值 data 的改变并不影响 msg
但 toRef 是对传入数据的引用，原始值 data 改变会影响 msg

- 正确书写：

```vue
<template>
	<div>{{ msg }}</div>
	<div>{{ data }}</div>
</template>
<script setup>
import { ref, toRefs, toRef } from "vue";
// 接受来自父组件的传参
const props = defineProps({
  data: String,
});
// 方法1：
const msg = toRef(props, 'data');
// 方法2：
const { data } = toRefs(props);
</script>
```

## 官网解释内容

[响应式 API：工具函数 | toRef](https://cn.vuejs.org/api/reactivity-utilities.html#toref)

