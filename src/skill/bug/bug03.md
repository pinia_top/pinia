---
title: 03.修改windows执行策略
date: 2023-06-30
---

# bug

windows 默认不允许 npm 全局命令执行脚本文件，所以需要修改执行策略

# 解决方法


1. 以`管理员身份`打开 `powershell` 命令行

2. 键入命令

   ```js
   set-ExecutionPolicy remoteSigned
   ```

3. 键入 A 然后敲回车 👌

4. 如果不生效，可以尝试重启