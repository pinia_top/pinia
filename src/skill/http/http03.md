---
title: 03.json-server
date: 2023-06-30
---

## json-server 简介

[npm 地址](https://www.npmjs.com/package/json-server) | [github 地址](https://github.com/typicode/json-server)

> Get a full fake REST API with **zero coding** in **less than 30 seconds** (seriously)

引用了官方的一句话，大概意思是 30 秒就能获得一套完整的模拟 REST API 接口。

使用 `json-server` 需要遵守一定的规范。

- 数据查询要使用 `GET`。
- 新增数据要使用 `POST`。
- 删除数据要使用 `DELETE`。
- 修改数据使用 `PUT` 和 `PATCH`。

其他啰嗦的介绍可以打开上面提供的[网址](https://link.juejin.cn?target=https%3A%2F%2Fgithub.com%2Ftypicode%2Fjson-server)看看。

## 30 秒起步

30 秒起步分 **4** 步完成：

1. `node` 环境安装
2. 安装 `json-server`
3. 创建数据库（其实就是一个 `json` 文件）
4. 启动服务

### 1. node 环境安装

`json-server` 需要通过 `npm` 下载，`npm` 依赖在 `node` 中。

`node` 常见的安装方法有 2 种。第一种是官方下载，第二种是使用 `nvm` 下载。自己选一种就行。

[node 官网，点击进入主页下载](https://link.juejin.cn?target=https%3A%2F%2Fnodejs.org%2Fen%2F)，下载完狂按“下一步”和“完成”就行了。

**注意：** `node` 版本一定要 `12` 以上。不然会报以下错误：

```bash
json-server requires at least version 12 of Node, please upgrade
```

### 2.安装 json-server

可以全局安装，也可以在某项目里安装。这里建议全局安装，因为以后你可能会对 `json-server` 产生依赖。

全局安装方式：

::: code-tabs#shell

@tab npm

```bash
npm install -g json-server
```

@tab yarn

```bash
yarn global add json-server
```

:::

**3. 创建数据库**

在你本机创建一个文件夹，然后新建一个 `json` 文件，再填入数据即可。

建议文件名不要出现中文。

例：

创建 `json-server-demo` 文件夹，在 `json-server-demo` 里创建 `db.json` 文件（这些文件夹和文件名都可以自由命名）。

`db.json` 文件录入以下数据（数据来自 `json-server` 官方文档，你也可以使用自己的数据）

```json
{
	"posts": [
		{
			"id": 1,
			"title": "json-server",
			"author": "typicode"
		}
	],
	"comments": [
		{
			"id": 1,
			"body": "some comment",
			"postId": 1
		}
	],
	"profile": {
		"name": "typicode"
	}
}
```

**4. 启动服务**

进入 `json-server-demo` 目录，打开终端输入以下命令即可

--watch:需要监听的 json 文件

--port:修改端口号

```bash
json-server --watch db.json --port 3000
```

## 进阶

### 启动参数

我们之前使用 `json-server --watch db.json` 这条命令启动了接口项目，其中 `json-server` 是服务启动的命令，`--watch` 是参数，`db.json` 是目标文件。

使用下面这条命令可以 查看配置项：

::: code-tabs#shell

@tab --help

```bash
json-server --help
```

@tab -h

```bash
json-server -h
```

:::

| 参数               | 简写  | 说明                                |
| ------------------ | ----- | ----------------------------------- |
| --config           | -c    | 指定配置文件                        |
| --port             | -p    | 端口号                              |
| --host             | -H    | 主机地址                            |
| --watch            | -w    | 监听文件                            |
| --routes           | -r    | 指定路由文件                        |
| --middlewares      | -m    | 指定中间件                          |
| --static           | -s    | 设置静态文件                        |
| --read-only        | --ro  | 只读                                |
| --no-cors          | --nc  | 禁用跨源资源共享                    |
| --no-gzip          | --ng  | 禁止 GZIP                           |
| --snapshots        | -S    | 设置快照目录                        |
| --delay            | -d    | 设置反馈延时（ms）                  |
| --id               | -i    | 设置数据的 id 属性（e.g. \_id）     |
| --foreignKeySuffix | --fks | 设置外键后缀（如 post_id 中的\_id） |
| --quiet            | -q    | 禁止输出日志消息                    |
| --help             | -h    | 显示帮助信息                        |
| --version          | -v    | 显示版本号                          |

使用 `-p` 或者 `--port` 配置端口号，例如配置 `6666` 端口

```bash
json-server -p 6666 db.json
```

启动后

```bash
\{^_^}/ hi!

Loading db.json
Done

Resources
http://localhost:6666/posts
http://localhost:6666/comments
http://localhost:6666/profile

Home
http://localhost:6666
```

### 主机地址

```bash
json-server --host 0.0.0.0 db.json
```

这里设置了 `0.0.0.0` ，之后通过本机 `IP` 来访问即可。同一个局域网内的其他设备也可以通过这个地址来访问。

### 配置路由

有时候我们的 `api地址` 可能不像上面所有案例中那么简单，此时就可以使用 **自定义路由** 的方法来模拟。

比如模拟下面这个接口：

```bash
http://localhost:3000/api/users/1
```

实现的步骤如下所示：

1. 创建 `routes.json` 文件（也可以不叫这个名字）
2. 启动服务时使用 `--routes` 参数

1、创建 `routes.json` ，并输入以下内容。

```json
{
	"/api/*": "/$1"
}
```

2、启动服务

```bash
json-server db.json --routes routes.json
```

3、访问

```bash
http://localhost:3000/api/posts
```

### 生成动态数据

如果我们要模拟 100 条数据，甚至更多的话，创建 `json` 文件然后一条一条录入的方式真的很不合时。

此时我们可以使用 `js` 通过循环的方式来实现数据创建。

1、首先在根目录下创建一个 `db.js` 的文件。

2、输入一下内容

```js
js复制代码module.exports = () => {
	const data = { users: [] };
	// 创建 100 个 users
	for (let i = 0; i < 100; i++) {
		data.users.push({ id: i, name: `user${i}` });
	}
	return data;
};
```

3、运行 `js` 文件

```bash
json-server db.js
```

4、查询一下

```bash
http://localhost:3000/users
```

### 查询整个数据库

访问以下地址可以获得整个数据库的数据。

```bash
http://localhost:3000/db
```

### 远程模式

如果想使用互联网上的数据，也可以直接运行 `json-server` 然后加上远端的地址即可。

```bash
json-server http://jsonplaceholder.typicode.com/db
```
