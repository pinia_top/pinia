---
title: 01.网络的正确学习方式
date: 2023-06-02
---

## 网络的正确学习方式

- 五层网络模型
- TCP/IP
- HTTP
- Postman/APifox
- AJAX
- 跨域及其解决方案
- JWT
- Cookie
- session
- 文件上传
- 文件下载
- 缓存协议
- CSRF
- XSS
- 网络性能优化
- 分片传输
- 域名与DNS
- SSL/TLS/HTTPS
- HTTP2
- WebStorket