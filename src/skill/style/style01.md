---
title: 01.旋转走马灯效果
date: 2023-05-30
---

::: normal-demo  旋转走马灯效果

```html
<body>
	<div class="ellipse"></div>
</body>
```

```css
    :root{
        --background-color: #000;
        --ellipse-color: #fff;
        --gradient-color: linear-gradient(45deg, #f3ec78, #af4261);
        --border-width: 10px;
    }
    body {
        background: var(--background-color);
    }
    .ellipse{
        width: 600px;
        height: 300px;
        background: var(--ellipse-color);
        border-radius: 50%;
        margin: 0 auto;
        position: relative;
        overflow: hidden;
    }
    .ellipse::before{
        content: '';
        position: absolute;
        width: 200%;
        height: 200%;
        background: var(--gradient-color);
        top: 50%;
        left: 50%;
        z-index: 0;
        transform-origin: 0 0;
        animation: move 3s linear infinite;
    }
    .ellipse::after{
        content: '';
        position: absolute;
        top: var(--border-width);
        left: var(--border-width);
        width: calc(100% - 2*var(--border-width));
        height: calc(100% - 2*var(--border-width));
        background: var(--background-color);
        border-radius: inherit;
    }
    @keyframes move {
        to {
            transform: rotate(1turn);
        }
    }
```



:::