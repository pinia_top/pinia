---
title: 06.ES6函数常见的操作(ES6)
date: 2023-05-27
---

## 场景一：创建日期对象

```js
  //从其他地方拿到的日期
  const date = [2023, 0x05, 27]
  //用上面的日期创建一个Date对象
  const d1 = new Date(date[0], date[1], date[2])
  const d2 = new Date(...date)
  console.log(d1,d2)
```

## 场景二：函数防抖

```js
  /**
   * 函数防抖
   * @param {Function} fn - 要进行防抖的目标函数
   * @param {Number} delay - 延迟时间，单位毫秒
   */
  // function debounce(fn, delay = 1000) {
  //   let timer = null
  //   const args1 = Array.prototype.slice.call(arguments, 2)
  //   return function () {
  //     clearTimeout(timer)
  //     const args2 = Array.prototype.slice.call(arguments)
  //     const args = args1.concat(args2)
  //     timer = setTimeout(() => {
  //       fn.apply(this, args)
  //     }, delay)
  //   }
  // }
  function debounce(fn, delay = 1000,...args1) {
    let timer = null
    return (...args2) => {
      clearTimeout(timer)
      timer = setTimeout(() => {
        fn(...args1,...args2)
      }, delay)
    }
  }

  const newFn = debounce(function (a, b, c, d, e) {
      console.log('防抖成功', a, b, c, d, e)
    },
    2000,
    1,//绑定function第一个参数
    2 //绑定function第二个参数
  )
  newFn(3, 4) //防抖成功 1 2 3 4 undefined
```

## 场景三：封装计时器

```js
  class Timer {
    constructor(callback, duration = 1000) {
      this.callback = callback
      this.duration = duration
      this.count = 0
      this.id = null
    }

    // start() {
    //   if (!this.id) {
    //     let that = this
    //     this.id = setInterval(function () {
    //       that.callback(++that.count)
    //     }, this.duration)
    //   }
    // }

    stop() {
      clearTimeout(this.id)
      this.id = null
    }

    start() {
      if (!this.id) {
        this.id = setInterval(() => {
          this.callback(++this.count)
        }, this.duration)
      }
    }


  }

  const t = new Timer(function (count) {
    console.log(count)
  }, 1000)
  t.start()
  setTimeout(function () {
    t.stop()
  }, 5000)
```

## 场景四：数组去重

```js
   /**
   * 数组去重
   */
  const arr = [1, 2, 3, 4, 5, 6, 7, 8, 1, 2];
  const arr2 = [...new Set(arr)];
  const arr3 = Array.from(new Set(arr));
  console.log(arr2,arr3);//[1, 2, 3, 4, 5, 6, 7, 8]

```

## 场景五：默认配置

```js
 function foo (options) {
   const defaultOptions = {
     a: 1,
     b: 2,
     c: 3
   }
   options = options|| {}
   options.a = options.a || defaultOptions.a
   options.b = options.b || defaultOptions.b
   options.c = options.c || defaultOptions.c
   console.log(options)
 }
 foo()
 foo({a: 10, b: 20})
//简单写法
 function foo (options={}) {
   const defaultOptions = {
     a: 1,
     b: 2,
     c: 3
   }
   options = {
     ...defaultOptions,
     ...options
   }
   console.log(options)
 }
 foo()
 foo({a: 10, b: 20})
```

## 场景六:单独使用函数打印日志

```js
  //在不改变下面代码的情况下，实现调用函数时进行日志记录
  const calculator = {
    count: 0,
    next() {
      return ++this.count
    },
    double(a) {
      return a *= 2
    },
    add(a, b) {
      return a + b
    }
  }
  // for (const key in calculator) {
  //   if(Object.hasOwnProperty.call(calculator,key)){
  //     const fn = calculator[key]
  //     console.log(fn)
  //     calculator[key] = function(){
  //       console.log(`日志记录开始：${key}`)
  //       fn.apply(this,arguments)
  //       console.log(`日志记录结束：${key}`)
  //
  //     }
  //   }
  // }
  for (const key in calculator) {
    const fn = calculator[key]
    calculator[key] = function (...args) {
      console.log(`开始调用函数：${key}(${args})`) //开始调用函数：add(1,2)
      fn.call(this, ...args)
      console.log(`结束调用函数：${key}`) //结束调用函数：add
    }
  }
  calculator.add(1, 2)
```

