---
title: 01.组件的意义
date: 2023-06-05
---

## vue

- vue最核心的俩个概念
  - 数据响应式
  - 组件化

## 组件

- 如果没有组件
  - 开发单元过大
  - 重复区域难以分离
- 组件就是从UI中抽离出来的一块区域

::: vue-playground 组件

@file Root.vue

```vue
<div>
    <Header></Header>
    <Main></Main>
    <Aside></Aside>
</div>
```

@file Header.vue

```vue
<div>
    <a href='' class='logo'>header</a>
    <div class='menu'>
        234
    </div>
</div>
```

@file Main.vue

```vue
<div>
    <Article></Article>
    <Article></Article>
</div>
```

@file Aside.vue

```vue
<div>
    <Item></Item>
    <Item></Item>
    <Item></Item>
</div>
```

@file Aside-item.vue

```vue
<div>
    item
</div>
```

@file Main-article.vue

```vue
<div>
    article
</div>
```

:::

- 组件的意义

  - 降低重复开发粒度，从而降低了整体复杂度

  - 减少了重复代码，从而提升了开发效率和可维护性

  - 更有利于团队协作

  - 更容易抽离公共组件